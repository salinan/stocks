<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Exceptions extends CI_Exceptions {

        
        function show_error($heading, $message, $template = 'error_general', $status_code = 500)
	{  
            if(config_item('debug_error')==TRUE){
                 // by leon
                 logline($message);
                 remotedebug('error',$GLOBALS['loglines']);
                 $GLOBALS['loglines']=array();
            }
 
            set_status_header($status_code);
            $message = '<p>'.implode('</p><p>', ( ! is_array($message)) ? array($message) : $message).'</p>';

            if (ob_get_level() > $this->ob_level + 1)
            {
                    ob_end_flush();
            }
            ob_start();
            include(APPPATH.'errors/'.$template.'.php');
            $buffer = ob_get_contents();
            ob_end_clean();
            return $buffer;
        }
    
    
        
	function show_php_error($severity, $message, $filepath, $line)
	{
           
            if(config_item('debug_php_error')==TRUE){
                // by leon
                $s = FriendlyErrorType($severity) . " ($severity) ";
                logline("$s @ line $line<br>$message<br>$filepath");
                remotedebug('php error',$GLOBALS['loglines']);
                $GLOBALS['loglines']=array();
            }
            
            $severity = ( ! isset($this->levels[$severity])) ? $severity : $this->levels[$severity];

            $filepath = str_replace("\\", "/", $filepath);

            // For safety reasons we do not show the full file path
            if (FALSE !== strpos($filepath, '/'))
            {
                    $x = explode('/', $filepath);
                    $filepath = $x[count($x)-2].'/'.end($x);
            }

            if (ob_get_level() > $this->ob_level + 1)
            {
                    ob_end_flush();
            }
            ob_start();
            include(APPPATH.'errors/error_php.php');
            $buffer = ob_get_contents();
            ob_end_clean();
            echo $buffer;
	}

}
// END Exceptions Class

/* End of file Exceptions.php */
/* Location: ./system/core/Exceptions.php */