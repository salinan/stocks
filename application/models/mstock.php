<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Mstock MODEL
 * 
 * Description...
 * 
 * @package mstock
 * @author leon 
 * @version 0.0.0
 */

class Mstock extends CI_Model {

	public function __construct()
	{
		parent::__construct();

	
	}
/**
 * QUERIES
 * select-where-get-result_array /query-result_array
 * where-update
 * select-from-where->count_all_results
 * SELECT
 * $this->db->where(array('name'=>$name))
 * $this->db->where_in('id',$arr_ids)
 * $this->db->from('table') OF $this->db->get('table',0,10)
 * $this->db->result_array();
 * $this->db->from("table")->count_all_results();
 * $this->db->query($SQL)->result_array(); // GEEN GET
 * CHANGE
 * $this->db->where_in('id',$arr_ids)->delete('table');
 * $this->db->[update/insert]_batch('table',$arr_assoc,'id');
 * $this->db->[update/insert]('table',array('value'=> $value));
 */
        ####################################################################################
        // plotting :
        // lees de stockdata 
        public function get_stockdata(
            $stockid    = NULL,
            $stockname  = NULL,
            $datefrom   = NULL,
            $dateuntil  = NULL,
            $strsma     = '',
            $function   = NULL
                ){
            
             //return json_encode([$stockid,$stockname,$datefrom,$dateuntil,$function]);
            
            $smas = explode('.',$strsma);
            $smaselect = array();
            // the values to be plotted                     
            $values = array(
                $stockname => array()
            );
            if($strsma){
                foreach ($smas as $sma) {
                    $smaselect[] = "data_average.sma$sma";
                    $values["sma$sma"] = array();
                }
            }
            $dataselect = array('data.id','data.time','data.date','data.close');
            $datawhere  = array('stockid'=>$stockid,'date >='=>$datefrom,'date <='=>$dateuntil);

           // return json_encode([$datefrom,$dateuntil]);
            
            $digits     = config_item('plot_decimals');   // afronden van alle data
            $xaxisdates = config_item('plot_xaxisdates'); // hoeveel datumlijnen staan er op de x-as
            $precision  = config_item('plot_precision');  // hoeveel meetpunten bevat de grafiek ongeveer
            // hoeveel records in totaal tussen de twee datums
            $this->db->where($datawhere);
            $total = $this->db->from('data')->count_all_results();

            // selecteer nu mbv de precisie de dagen/meetpunten
            // hoeveel records in totaal tussen de twee datums
            
            $steps = $total;
            $this->db->where($datawhere);
            if($total>$precision){
                $steps = round($total/$precision);
                $this->db->where("data.id mod $steps = 0");
            }
            $this->db->select($dataselect);
            if($strsma){
                $this->db->select($smaselect);
                $this->db->join('data_average', 'data_average.id = data.id');
            }
            $this->db->from('data');
            $this->db->order_by('data.date','asc');
            $query = $this->db->get();
            $ar = $query->result_array();
            $query->free_result();
            
            
            ################################################################
            
            $newquery = FALSE;
            if($strsma){
                foreach ($smas as $sma) {
                    $ar_sma=array(); 
                    foreach ($ar as $item) {
                        if($item["sma$sma"]==0){
                            $ar_sma[]=
                            array('id'=>$item['id'],'date'=>$item['date']);
                        }
                    }
                    if(count($ar_sma)){
                        $newquery = TRUE;
                        // bereken sma 
                        $this->update_sma_from_idlist($stockid,$sma,$ar_sma);
                    } 
                }
            }

            ################################################################
            
            if($newquery==TRUE){
                $this->db->where($datawhere);
                if($total>$precision){
                    $steps = round($total/$precision);
                    $this->db->where("data.id mod $steps = 0");
                }
                $this->db->select($dataselect);
                if(count($smaselect)){
                    $this->db->select($smaselect);
                    $this->db->join('data_average', 'data_average.id = data.id');
                }
                $this->db->from('data');
                $this->db->order_by('data.date','asc');
                $query = $this->db->get();
                $ar = $query->result_array();
                $query->free_result();
            }

            if($function=='data'){
                return $ar;
            }
            
            
            //q();
            ###############################################################
            #### 
            ###############################################################
            ## Indien voor een periode voor alle dagen     
            ## dayquotes beschikbaar zijn wordt overgeschakeld 
            ## naar deze tabel. voor alle waarden worden de  
            ## average overgenomen van die dag          
            ##
            ###############################################################
        
        
//            
//            // verzamel alle plotdatums
//            $dates = array();
//            foreach ($ar as $item) {
//                 $dates[] = $item['date'];
//            }
//            // indien vandaag : voeg ook toe
//            $dates[]=$dateuntil;
//            $this->db->select(array('id','time','date','close'));
//            $this->db->where(array('stockid'=>$stockid));
//            $this->db->where_in('date',$dates);
//            $this->db->from('data_interval');
//            $query = $this->db->get();
//            $ar5 = $query->result_array();
//            $query->free_result();
//             // q();
//              
//            if(count($ar5)){
//               // p($ar5);
//                $ar = array_merge($ar,$ar5);
//                function cmp($a,$b){
//                    return $a['time']-$b['time'];
//                }
//                usort($ar,"cmp");
//            }

            ###########################################

            $ar4 = array();
            $minvalue = 100000;
            $maxvalue = 0;
            $xaxisstep = round(count($ar)/$xaxisdates);
            foreach ($ar as $key => $item) {
                $value = round($item['close'],$digits);
                $values[$stockname][] = array($key,$value);
                if($strsma){
                    foreach ($smas as $sma) {
                        $avg = round($item["sma$sma"],$digits);
                        $values["sma$sma"][] = array($key,$avg);
                    }
                }
                // bereken de hoogste en laagste waarde
                if($minvalue>$value){$minvalue = $value;}
                if($maxvalue<$value){$maxvalue = $value;}
                if($key%$xaxisstep==0){
                    $ar4[]= array($key,
                    date("d-m-Y", strtotime($item['date'])));
                }
            }
            $values2 = array();
            foreach ($values as $key => $item) {
                if($key==$stockname){
                     $values2[] =  array(
                         'label'=>$key,
                         'data'=>$item,
                          'color'=> "rgba(0, 152, 251, 1)",
                         'lines'=>array(
                              'fill'=>true,
                              'lineWidth'=>0.8,
                              'fillColor'=> "rgba(5, 250, 189, 0.07)"
                             
                             ));
                }
                else{
                    $values2[] =  array('label'=>$key,'data'=>$item,);
                }
               
               
            }
            
            
            $data = array(
                'count' => count($ar),
                'min'   => $minvalue,
                'max'   => $maxvalue,
                'xaxis' => $ar4,
                'graph' => $values2
            );
            return json_encode($data);
        }
         
        
          // TERUG IN DE TIJD (EVT ZONDER DIE DAG ZELF)
        public function get_historical_data___(
            $name=NULL,$days=1,$date=NULL,$excludedate=FALSE,$fields=NULL){
            $id = $this->get_stockid_byname($name);
            if(!$id||!$date){return NULL;}
            if($fields){$this->db->select($fields);}
            $qdate = $excludedate===TRUE?'date <':'date <=';
            $this->db->order_by('date','desc');
            $this->db->where(array('stockid'=>$id, $qdate => $date));
            $this->db->limit($days)->from('data');
            $query = $this->db->get();
            $ar = $query->result_array();
            $query->free_result();
            return array_reverse($ar);
        }
        
        
        
        #####################################################################################
        
        
        // get some additional info for every stock
        public function stock_get_extrainfo(){
            return;
            $ar = $this->db->from('stocks')->get()->result_array();
            $tickers = array();
            foreach ($ar as $item){
                $ticker = $item['yahoo'];
                if(!$ticker){
                    continue;
                }
                $tickers[]=$ticker;  
            }
            $tickers = implode(',',$tickers);
            $url = "http://finance.yahoo.com/d/quotes.csv?e=.csv&f=n0s0x0c4&s=$tickers";
            $ar = explode(PHP_EOL,get_page($url));
            $data = array();
           
            foreach ($ar as &$item) {
                $item = preg_replace('/"/','',$item);
                $item = trim($item);    
                $item = explode(',',$item);
                if(count($item)>0&&$item[0]){
                    $data[]=array(
                        'name'    => $item[0],
                        'yahoo'   => $item[1],
                        'exchange'=> $item[2],
                        'currency'=> $item[3]
                    );
                }
                
            } 
            $this->db->update_batch('stocks',$data,'yahoo');
        }
        
        
        // get the isin for every stock on uk.finance.yahoo.com
        public function stock_get_isin_extrainfo(){
            $this->db->where(array('isin'=>''));
            $ar = $this->db->from('stocks')->get()->result_array();
            $tickers = array();
            $data = array();
            foreach ($ar as $item){
                $ticker = $item['yahoo'];
                $id     = $item['id'];
                if($ticker){
                   $url = "https://uk.finance.yahoo.com/q?s=$ticker";
                   $html = get_page($url);
                   $ar2  = get_nodes($html,array(
                              "//span[@class='rtq_exch']"
                           ));
                   if(isset($ar2[0][0])&&$ar2[0][0]&& preg_match('/ISIN\:\s*(\w+)/',$ar2[0][0],$m)){
                       $isin = $m[1];
                   }else{
                       $isin = 'N/A';
                   }
                   $data[] = array(
                      'isin'=> $isin,
                       'id' => $id
                    );
                }
            }
            $this->db->update_batch('stocks',$data,'id');
        }
        
        //get just the ticker for this stock

        public function get_yahooticker($ticker){
            //Name                  n0
            //Symbol	            s0
            //Last Trade Date	    d1 (date)
            //Open                  o0 (open)
            //Days High             h0 (high)
            //Days Low              g0 (low)
            //Last Trade Price Only l1 (close)
            //Volume                v0 (volume)
            //Currency	            c4
            //Stock Exchange	    x0
            $url = "http://finance.yahoo.com/d/quotes.csv?e=.csv&f=n0s0d1c4x0l1&s=$ticker";
            $str = sendrequest($url);
            $str = preg_replace('/","?/','|',$str);
            $str = trim($str,'"');
            $str = preg_replace('/,/','',$str);
            $str = trim($str);
            $ar = explode('|',$str);
            $str = preg_replace('/\|/',', ',$str);
            foreach ($ar as &$item) {
                $item = trim($item,'" ');
                $item = trim($item);
            }
            $date     = $ar[2];
            $ticker   = $ar[1];
            $name     = $ar[0];
            $currency = $ar[3];
            $exchange = $ar[4];
            $close    = $ar[5];
            if($close==0){
                return 'Stock does not exist : '.$str;
            }
            $data = array(
                'yahoo'    =>  $ar[1],
                'name'     =>  $ar[0],
                'currency' =>  $ar[3],
                'exchange' =>  $ar[4]
            );
            if($this->db->insert('stocks',$data)){
               return 'Stock is added : '.$str;
            } 
            return 'error';
        }
        public function test_yahooticker($ticker) {
            $this->db->where(array('yahoo'=>$ticker));
            return $this->db->from('stocks')->count_all_results();
        }
        public function update_yahooticker($id,$value) {
            $this->db->where(array('id'=> $id));
            $this->db->update('stocks',array('yahoo'=>$value));
        }
        public function update_stockname($id,$value) {
            $this->db->where(array('id'=> $id));
            $this->db->update('stocks',array('name'=>$value));
        }
        public function update_beursnlticker($id,$value) {
            $this->db->where(array('id'=> $id));
            $this->db->update('stocks',array('beursnl'=>$value));
        }
        public function delete_stock($id=NULL) {
            if(!$id){return;}
            $this->db->where(array('stockid'=> $id));
            $this->db->delete('data');
            $this->db->where(array('id'=> $id));
            $this->db->delete('stocks');
        }
        public function update_stockstatus($id,$status) {
            $this->db->where(array('id'=> $id));
            $this->db->update('stocks',array('status'=>$status));
        }
        
        public function get_all_stocks(){
            $this->db->order_by('exchange','asc');
            $this->db->order_by('name','asc');
            
            $ar = $this->db->from('stocks')->get()->result_array();
            return $ar;
        }
        public function get_active_stocks(){
            $this->db->where(array('status'=>'Actief'));
            $this->db->order_by('name','asc');
            $ar = $this->db->from('stocks')->get()->result_array();
            return $ar;
        }
        

        public function get_alertvalue($id=NULL,$always=NULL){
            if($always==NULL){$this->db->where(array('send'=>'No'));}
            $ar = $this->db->where(array('alertid'=> $id))
            ->limit(1)->from('alerts')->get()->result_array();
            return count($ar)?$ar[0]['value']:'';
        }
        
        public function get_alert($id=NULL){
            $ar = $this->db->where(array('alertid'=> $id))
            ->limit(1)->from('alerts')->get()->result_array();
            return count($ar)?$ar[0]:array('send'=>'No','value'=>'');
        }
        
        
        public function set_alertvalue($id=NULL,$value=NULL){
            if($this->db->where(array('alertid'=>$id))
            ->from('alerts')->count_all_results()){
                $this->db->where(array('alertid'=>$id));
                $this->db->update('alerts',array('value'=> $value,'send'=>'No'));
            }else{
                $this->db->insert('alerts',
                array('alertid'=>$id,'value'=> $value,'send'=>'No'));
            }
        }
        public function get_alerts(){
            $ar = $this->db->from('alerts')->
            where(array('value !='=>''))
            ->get()->result_array();
            $ar2 = array();
            foreach ($ar as $item) {
                $ar2[$item['alertid']]=$item['value'];
            }
            return $ar2;
        }
        public function set_alertsend($id=NULL){
             if($this->db->where(array('alertid'=>$id))
             ->from('alerts')->count_all_results()){
                $this->db->where(array('alertid'=>$id));
                $this->db->update('alerts',array('send'=>'Yes'));
             }
        }
        // insert a new stock into the stocks table
        public function set_stock($type=NULL,$name=NULL){
              if(!$this->db->where(array('name'=>$name))
             ->from('stocks')->count_all_results()){
                $this->db->insert('stocks',
                array('name'=>$name,'type'=> $type));
             }
        }
        public function set_stock2($name=NULL,$behr_profile,$behr_data){
              p([$name,$behr_profile,$behr_data]);
              $this->db->where(array('name'=>$name));
              $this->db->update('stocks',array('behr_profile'=>$behr_profile,'behr_data'=>$behr_data));
        }
        
        
        // add or remove rows when needed in the 2 secondary datatables
        
        
        
        public function cascade_secondary_datatables(){
            $tables = array(
                'data_average',
                'data_lowhigh'
            );
            foreach ($tables as $table) {
                // add to data_...
                $ar = $this->db->query(
                "SELECT `data`.`id` FROM `data` WHERE (`data`.`id`) "
              . "NOT IN (SELECT `$table`.`id` FROM `$table`);")->result_array();
                foreach ($ar as &$item) {
                    $item = $item['id'];
                }
                asort($ar,SORT_NUMERIC);
                foreach ($ar as &$item) {
                    $item = array('id'=>$item);
                }
                $ar = array_chunk($ar,500);
                foreach ($ar as $ar2){
                    $this->db->insert_batch($table,$ar2);
                    p($this->db->affected_rows());
                }
                // remove from data_...
                $ar = $this->db->query(
                "SELECT`$table`.`id` FROM `$table` WHERE (`$table`.`id`) "
              . "NOT IN (SELECT `data`.`id` FROM `data`);")->result_array();              
                foreach ($ar as &$item) {
                    $item = $item['id'];
                }
                $ar = array_chunk($ar,500);
                foreach ($ar as $ar2){
                    $this->db->where_in('id',$ar2)->delete($table);
                     p($this->db->affected_rows());
                }
                break;
            }
        }
        
        // days that are missed in cronjobs will be filled in 
        public function yahoo_get_historical_data($stockid,$date){
           // doorloop de dagen van een periode en haal er de werkdagen uit
           $ar = $this->db->select('id')->from('stocks')->get()->result_array();
           $allstocks = get_assoc($ar);
           $stockdays = workingdays(31);
           foreach ($stockdays as $item) {
               $ar = $this->db->select('stockid')->where(array('date'=> $item,'close'))->
               from('data')->get()->result_array();
               $filledstocks = get_assoc($ar);
               p([$item,$filledstocks])  ;
               
           }
            
            
            return;
            
            
            
           
            
            
           
           $tmp = explode('-',$date);
           $y = $tmp[0]; 
           $m = $tmp[1]-1;
           $d = $tmp[2];
            debug();
           $url = "http://ichart.yahoo.com/table.csv?s=$stockid&a=$m&b=$d&c=$y&d=$m&e=$d&f=$y&g=d&ignore=.csv";
           $ar = explode(PHP_EOL,get_page($url));
                p($ar);
           if(isset($ar[1])){
                $item = explode(',',$ar[1]);
                $data = array(
                    'stockid' => $stockid,
                    'time'    => strtotime($item[0]),
                    'date'    => $item[0],
                    'open'    => $item[1],
                    'high'    => $item[2],
                    'low'     => $item[3],
                    'close'   => $item[4],
                    'volume'  => $item[5]
                );
                 p($data);
           }
           
          
                  
        }
        
        
        
        
        // get all the historical data of all new added stocks
        // these have set : dataimport = No
        
        public function yahoo_historical_data_new_stock(){
            $this->db->where(array('dataimport'=>'No'));
            $ar = $this->db->from('stocks')->get()->result_array();
            $msg = array();
            foreach ($ar as $item){
                $ticker = $item['yahoo'];
                $stockid = $item['id'];
                $name = $item['name'];
                if(!$ticker){
                    continue;
                }
                $nodata = $this->db->where(array('stockid' => $stockid))->
                from('data')->count_all_results()==0?TRUE:FALSE;
                if($nodata){
                    $url = "http://ichart.yahoo.com/table.csv?s=$ticker";
                    $d = explode(PHP_EOL,sendrequest($url));
                    foreach ($d as $key2 => &$item2){
                        if($item2==''||$key2==0){
                            unset($d[$key2]);
                        }
                    }
                    $d = array_reverse(array_values($d));
                    $data2=array();
                    foreach ($d as $key2 => &$item2){
                        $item2 = explode(',',$item2);
                        $data = array(
                            'stockid' => $stockid,
                            'time'    => strtotime($item2[0]),
                            'date'    => $item2[0],
                            'open'    => $item2[1],
                            'high'    => $item2[2],
                            'low'     => $item2[3],
                            'close'   => $item2[4],
                            'volume'  => $item2[5]
                        );
                        $data2[]=$data;
                    }
                    $count= count($data2);
                    if($count){
                        $data2 = array_chunk($data2,500);
                        foreach ($data2 as $data3) {
                           $this->db->insert_batch('data',$data3);
                        }
                        $this->db->where(array('id'=>$stockid));
                        $this->db->update('stocks',array('dataimport'=>'Yes'));
                    }
                    $msg[] =  "$count records historical data added for $name";
                }
            }
            if(!count($msg)){
                return "no historical data was added";
            }
            return implode (' - ',$msg);
        }
        
        
        
        // werk AL de dagelijkse koersen bij met 1 request !!!
 
        public function yahoo_get_quote_data(){
            //$this->db->where(array('status'=>'Actief'));
            $ar = $this->db->from('stocks')->get()->result_array();
            $tickers = array();
            foreach ($ar as $item){
                $ticker = $item['yahoo'];
                if(!$ticker){
                    continue;
                }
                $tickers[]=$ticker;  
            }    
            
            //https://code.google.com/p/yahoo-finance-managed/wiki/enumQuoteProperty
            //Name                  n0
            //Symbol	            s0
            //Last Trade Date	    d1 (date)
            //Open                  o0 (open)
            //Days High             h0 (high)
            //Days Low              g0 (low)
            //Last Trade Price Only l1 (close)
            //Volume                v0 (volume)
            //Currency	            c4
            //Stock Exchange	    x0
            
            $tickers = implode(',',$tickers);
            $url = "http://finance.yahoo.com/d/quotes.csv?e=.csv&f=n0s0d1o0h0g0l1v0&s=$tickers";
            $ar = explode(PHP_EOL,sendrequest($url));
            foreach ($ar as $item) {   
                if($item){
                    $ar2 = explode(',',$item);
                    $date = explode('/',trim($ar2[2],'" '));
                    $date[0] = trailzero($date[0]);
                    $date[1] = trailzero($date[1]);
                    $date    =  $date[2].'-'.$date[0].'-'.$date[1];
                    $ticker  = trim($ar2[1],'" ');
                    $name    = trim($ar2[0],'" ');
                    $volume  = trim($ar2[7]);
                    // get the stockid with the ticker from the feed
                    $tmp = $this->db->where(array('yahoo'=>$ticker))->
                    limit(1)->from('stocks')->get()->result_array();
                    if(count($tmp)){
                        $data = array(
                            'stockid' => $tmp[0]['id'],
                            'time'    => strtotime($date),
                            'date'    => $date,
                            'open'    => (is_numeric($ar2[3])?$ar2[3]:0),
                            'high'    => (is_numeric($ar2[4])?$ar2[4]:0),
                            'low'     => (is_numeric($ar2[5])?$ar2[5]:0),
                            'close'   => (is_numeric($ar2[6])?$ar2[6]:0),
                            'volume'  => (is_numeric($volume)?$volume:0)
                        );
                        if($this->db->where(array('date'=>$date,'stockid' => $stockid))->
                            from('data')->count_all_results()==0){
                            $this->db->insert('data',$data);
                        }
                    }
                }
            }
        }
        
        // tijdens beursuren AEX elke 15 minuten een update naar de database
        
        public function yahoo_get_quote_data_interval(){
            $this->db->where(array('exchange'=>'Amsterdam'));
            $ar = $this->db->from('stocks')->get()->result_array();
            $tickers = array();
            foreach ($ar as $item){
                $ticker = $item['yahoo'];
                if(!$ticker){
                    continue;
                }
                $tickers[]=$ticker;  
            }    
            
            //https://code.google.com/p/yahoo-finance-managed/wiki/enumQuoteProperty
            //Name                  n0
            //Symbol	            s0
            //Last Trade Date	    d1 (date)
            //Open                  o0 (open)
            //Days High             h0 (high)
            //Days Low              g0 (low)
            //Last Trade Price Only l1 (close)
            //Volume                v0 (volume)
            //Currency	            c4
            //Stock Exchange	    x0
            
            $tickers = implode(',',$tickers);
            //$url = "http://finance.yahoo.com/d/quotes.csv?e=.csv&f=n0s0d1o0h0g0l1v0&s=$tickers";
            $url = "http://finance.yahoo.com/d/quotes.csv?e=.csv&f=n0s0d1l1v0a0b0&s=$tickers";
            $ar = explode(PHP_EOL,sendrequest($url));
            foreach ($ar as $item) {   
                if($item){
                    $ar2 = explode(',',$item);
                    $date = explode('/',trim($ar2[2],'" '));
                    $date[0] = trailzero($date[0]);
                    $date[1] = trailzero($date[1]);
                    $date    =  $date[2].'-'.$date[0].'-'.$date[1];
                    $ticker  = trim($ar2[1],'" ');
                    $name    = trim($ar2[0],'" ');
                    $volume  = trim($ar2[4]);
                    // ROND TIJD AF OP 5 MINUTEN
                    $time = round(time()/300)*300;  
                    // get the stockid with the ticker from the feed
                    $tmp = $this->db->where(array('yahoo'=>$ticker))->
                    limit(1)->from('stocks')->get()->result_array();
                    $stockid = $tmp[0]['id'];
                    if(count($tmp)){
                        $data = array(
                            'stockid' => $stockid,
                            'time'    => $time,
                            'date'    => $date,
                            'close'   => (is_numeric($ar2[3])?$ar2[3]:0),
                            'ask'    => $ar2[5],
                            'bid'    => $ar2[6],
                            'volume'  => (is_numeric($volume)?$volume:0)
                        );
//                        debug();
//                        p($data);
                        if($this->db->where(array('date'=>$date,
                            'stockid' => $stockid,'time'=>$time))->
                            from('data_interval')->count_all_results()==0){
                            $this->db->insert('data_interval',$data);
                        }
                    }
                }
            }
        }

        
        // try to get a stock by its name
        public function get_stockid_byname($name=NULL){
            $ar = $this->db->where(array('name'=>$name))->limit(1)
            ->from('stocks')->get()->result_array();
            return count($ar)?$ar[0]['id']:NULL;  
        }
        
        
        // set a value for this stock for this day
        public function set_stock_day_value($name=NULL,$date=NULL,$value=NULL){
            $id = $this->get_stockid_byname($name);
            $time = strtotime($date);
            if($id&&$date&&$value&&$time&&
            $this->db->where(array('stockid'=>$id,'date'=>$date))
            ->from('data')->count_all_results()==0){
                    $this->db->insert('data',
                    array('stockid'=>$id,'value'=> $value,'date'=>$date,'time'=>$time));
             }
        }
        
        public function get_stock_average($name=NULL,$days=10,$date=NULL,$type='SMA'){
            $id = $this->get_stockid_byname($name);
            $this->db->where(
            array('stockid'=>$id,'date'=>$date,'days'=>$days,'type'=>$type));
            $ar = $this->db->limit(1)->from('averages')->get()->result_array();
            return count($ar)?$ar[0]['value']:NULL;
        }
        
        
        
        
        
          // returns previous EMA
        public function get_stock($name=NULL,$days=10){
            $id = $this->get_stockid_byname($name);
            if(!$id){return;}
            $ar = $this->db->where(array('stockid'=>$id))
            ->from('data')->get()->result_array();

            foreach ($ar as &$item) {
                $date = $item['date'];
                $item["SMA$days"] = $this->get_stock_average($name,$days,$date,'SMA');
                $item["EMA$days"] = $this->get_stock_average($name,$days,$date,'EMA');
                
            }
           return $ar;
           
        }
        
        ######################## TRENDRICHTING ########################
        // bereken de huidige trendrichting van het sma
        // returns up,down,flat
        // hoe ver kijk je terug om de richting te bepalen?
        // een percentage van het aantal seconden
        public function get_trend_direction($stockid=NULL,$range=120){
            $dateright = date('Y-m-d');
            $pct_lb    = config_item('trend_direction_lookback');
            $days_lb   =  round(($pct_lb/100)*$range);
            $dateleft  = dateminusdays($days_lb,$dateright);
            $ar        = $this->get_stockdata($stockid,'stock',$dateleft,$dateright,$range,'data');
            $total     = 0;
            $count     = count($ar);
            // is het een even getal? zo nee maak het even, de oudste gaat eraf
            if(($count%2)!=0){
               array_shift($ar);
            }
            foreach ($ar as $item) {
                $total += $item['close'];
            }
            $average    = $total/$count;
            $margin     = config_item('trend_direction_swing');
            $downers    = 0;
            $uppers     = 0;
            $equals     = 0;
            foreach ($ar as $item) {
                if($item['close']>$average-(($margin/100)*$average)&&
                  $item['close']<$average+(($margin/100)*$average)){
                    $equals++;
                }else
                
                if($item['close']>=$average){
                    $uppers++;
                }else
                if($item['close']<=$average){
                    $downers++;
                } 
            }
            $upperspct = round(($uppers/$count)*100,1);
            $eqspct = round(($equals/$count)*100,1);
            $downersspct = round(($downers/$count)*100,1);

            p(array(
                'range' =>$range,
                'aantal' =>$count,
                'gemiddelde' =>$average,
                'margin' =>$margin,
                'downers' =>$downers,
                'equals' =>$equals,
                'uppers' =>$uppers,
                'pctdown' =>$downersspct,
                'pctequal' =>$eqspct,
                'pctup' =>$upperspct
                
            ));
          
        
        }
        
        
        
        
        ######################## SMA BEREKENING ########################
    
        
        
        // SELECTEER DE DATA VAN EEN FONDS VANAF DIE DAG 
        // TERUG IN DE TIJD (EVT ZONDER DIE DAG ZELF)
        public function get_historical_data(
            $name=NULL,$days=1,$date=NULL,$excludedate=FALSE,$fields=NULL){
            $id = $this->get_stockid_byname($name);
            if(!$id||!$date){return NULL;}
            if($fields){$this->db->select($fields);}
            $qdate = $excludedate===TRUE?'date <':'date <=';
            $this->db->order_by('date','desc');
            $this->db->where(array('stockid'=>$id, $qdate => $date));
            $this->db->limit($days)->from('data');
            $query = $this->db->get();
            $ar = $query->result_array();
            $query->free_result();
            return array_reverse($ar);
        }
        
        // GEBRUIK STOCKID IPV NAAM VOOR SELECTIE
        public function get_historical_data_2(
                $stockid=NULL,
                $days=1,
                $date=NULL,
                $excludedate=FALSE,
                $fields=NULL){
            if(!$stockid||!$date){return NULL;}
            if($fields){$this->db->select($fields);}
            $qdate = $excludedate===TRUE?'date <':'date <=';
            $this->db->order_by('date','desc');
            $this->db->where(array('stockid'=>$stockid,$qdate=>$date));
            $this->db->limit($days)->from('data');
            $query = $this->db->get();
            $ar = $query->result_array();
            $query->free_result();
            return array_reverse($ar);
        }
        
        
        // BEREKEN DE SMA MET GEBRUIK VAN DE HISTORISCHE DATA
        public function calculate_sma_from_historical_data(
                $stockid=NULL,
                $days=5,
                $date=NULL){
            $ar = $this->get_historical_data_2($stockid,$days,
                  $date,FALSE,array('close'));
            $value = 0;
            $count=count($ar);
            // GEEN SMA BEREKENEN ALS PERIODE KORTER IS 
            //if(!$count||$count<$days){return 0;}
            if(!$count){return 0;}
            
            foreach ($ar as $item) {
                $value += $item['close'];
            }
            unset($ar);
            $value = round($value/$count,6);
            return $value;
        }
        
        
        // BEREKEN DE LOWHIGH MET GEBRUIK VAN DE HISTORISCHE DATA
        public function calculate_lowhigh_from_historical_data(
                $stockid=NULL,
                $days=5,
                $date=NULL){
            $ar = $this->get_historical_data_2($stockid,$days,
                  $date,FALSE,array('close'));
            if(count($ar)<$days){return NULL;}
            $lowhigh = array(
                'low'  => 1000000,
                'high' => 0
            );
            $ok=FALSE;
            foreach ($ar as $item) {
                $value = $item['close'];
                if($value<$lowhigh['low']){
                    $lowhigh['low'] = $value;
                    $ok=TRUE;
                }
                if($value>$lowhigh['high']){
                    $lowhigh['high'] = $value;
                    $ok=TRUE;
                }
            }
            unset($ar);
            return $ok?$lowhigh:NULL;
        }
        
       // bereken sma voor 1 stock direct voor het plotten
        
        public function update_sma_from_idlist($stockid=NULL,$days=NULL,$ar=NULL){
            $data = array();
            foreach ($ar as $item) {
                $date = $item['date'];
                $sma  = $this->calculate_sma_from_historical_data($stockid,$days,$date);
                if($sma){
                    $data[]=array(
                        "sma$days" => $sma,
                        'id'       => $item['id']
                    );
                }
            }
            if(count($data)){
                $data = array_chunk($data,500);
                foreach ($data as $data2) {
                  $this->db->update_batch('data_average',$data2,'id');
                }
            }   
        }
        
        // BEREKEN ALLE SMA VOOR ALLE STOCKS !!!!!!
        // EN SCHRIJF NAAR DATATABEL
        
        public function update_simple_moving_averages(){
            $this->cascade_secondary_datatables();
            //debug();
            $total = 0;
            $ranges = config_item('ma_ranges'); 
            //$this->db->where(array("yahoo" => 'ASM.AS'));
            // SELECTEER STOCKS
            $ar = $this->db->select(array('id','name'))->
            from('stocks')->get()->result_array();
            //p($ar);q();
            foreach ($ar as $item) {
                $stockname  = $item['name'];
                $stockid    = $item['id'];
                //p($item);
                // SELECTEER NU VAN DIT STOCK ALLE DATA WAAR NOG GEEN SMA BEREKEND IS
                foreach ($ranges as $days) {
                    $data=array();
                    $this->db->where(array("data_average.sma$days" => '0'));
                    $this->db->where(array('data.stockid'=>$stockid));
                    $this->db->from('data');
                    $this->db->select(array('data.id','data.date'));
                    $this->db->order_by('data.date','asc');
                    $this->db->join('data_average', 'data_average.id = data.id');
                    $query = $this->db->get();
                    $ar2 = $query->result_array();
                    $query->free_result();
                    //p(count($ar2));q();
                    //p($ar2);                
                    foreach ($ar2 as $key2 => &$item2) {
                        $date = $item2['date'];
                        // bereken voor deze specifieke dag de sma MBV STOCKID
                        $sma = $this->calculate_sma_from_historical_data($stockid,$days,$date);
                        ///q();
                        if($sma){
                            $data[]=array(
                                "sma$days" => $sma,
                                'id'       => $item2['id']
                            );
                        }
                    }
                    if(count($data)){
                        $total += count($data);
                        $data = array_chunk($data,25);
                        foreach ($data as $data2) {
                          $this->db->update_batch('data_average',$data2,'id');
                           //q();
                        }
                    }
                    unset($data);
                    unset($ar2);
                    $this->db->insert('test',array('name'=> $stockname,'range'=>$days,'type'=>'sma'));
            
                }
                
            }
            return $total ;
        }
        
        
        ################################################################
        
        
        
        public function update_low_and_high_averages(){
            //debug();
            $this->cascade_secondary_datatables();
            $total = 0;
            $ranges = config_item('ma_ranges');
            //$this->db->where(array("yahoo" => 'ASM.AS'));
            // SELECTEER STOCKS
            $ar = $this->db->select(array('id','name'))->
            from('stocks')->get()->result_array();
            foreach ($ar as $item) {
                $stockname  = $item['name'];
                $stockid    = $item['id'];
              
                // SELECTEER NU VAN DIT STOCK ALLE DATA WAAR NOG GEEN SMA BEREKEND IS
                foreach ($ranges as $days) {
                    $data=array();
                    // we test both low and high if they are empty
                    $this->db->where(array("data_lowhigh.low$days" => '0',"data_lowhigh.high$days" => '0'));
                    $this->db->where(array('data.stockid'=>$stockid));
                    $this->db->from('data');
                    $this->db->select(array('data.id','data.date'));
                    $this->db->order_by('data.date','asc');
                    $this->db->join('data_lowhigh', 'data_lowhigh.id = data.id');
                    $query = $this->db->get();
                    $ar2 = $query->result_array();
                    $query->free_result();
                    foreach ($ar2 as &$item2) {
                        $date = $item2['date'];
                        // bereken voor deze specifieke dag de low high's
                        $lowhigh = $this->calculate_lowhigh_from_historical_data($stockid,$days,$date);
                        if($lowhigh){
                            $data[]=array(
                                "low$days"  => $lowhigh['low'],
                                "high$days" => $lowhigh['high'],
                                'id'        => $item2['id']
                            );
                        }
                    }
                    //p($data);
                    if(count($data)){
                        $total += count($data);
                        $data = array_chunk($data,25);
                        foreach ($data as $data2) {
                              $this->db->update_batch('data_lowhigh',$data2,'id');
                        }
                    }
                    unset($data);
                    unset($ar2);                
                    $this->db->insert('test',array('name'=> $stockname,'range'=>$days,'type'=>'lowhigh'));
                }
            }
            return $total ;
        }
        
        
        ################################################################
        
        
        
        
        
        
        // returns previous EMA  DEZE FUNCTIE WERKT NOG NIET GOED !!!!!!
        public function get_previous_ema($name=NULL,$days=10,$date=NULL){
            return;
            $id = $this->get_stockid_byname($name);
            if(!$id){return;}
            $this->db->order_by('date','desc');
            $this->db->where(
            array('stockid'=>$id,'date <'=>$date,'days'=>$days,'type'=>'EMA'));
            $ar = $this->db->limit(1)->from('averages')->get()->result_array();
            return count($ar)?$ar[0]['value']:NULL;
        }
        
        // calculate a moving averages SMA and EMA
        public function set_averages($name=NULL,$days=10){
            return;
            $id = $this->get_stockid_byname($name);
            if(!$id){return;}
            // first get all the stockdata, as we will loop through all the data 
            $this->db->order_by('date','asc');
            $ar = $this->db->where(array('stockid'=>$id))
            ->from('data')->get()->result_array();
            foreach ($ar as $item) {
                $date = $item['date'];
                $currentvalue = $item['value'];
                ########## Simple Moving Average (SMA) ###########
                // get x 'days' records before the date of this date (if possible)
                $this->db->order_by('date','desc');
                $ar2 = $this->db->limit($days)-> 
                where(array('stockid'=>$id,'date <='=>$date))
                ->from('data')->get()->result_array();
                $count = count($ar2);
                if($count==$days){
                    $sum = 0;
                    foreach ($ar2 as $item2) {
                        $sum += ($item2['value']-0);                    
                    }
                    $sma = round(($sum/$count),6);
                    if($this->db->where(array('stockid'=>$id,'date'=>$date,'days'=>$days,'type'=>'SMA'))
                    ->from('averages')->count_all_results()==0){
                        $time = strtotime($date);
                        $this->db->insert('averages',array(
                            'stockid' => $id,
                            'days'    => $days,
                            'value'   => $sma,
                            'date'    => $date,
                            'time'    => $time,
                            'type'    => 'SMA')); 
                    }
                }
                


//In this example we shall calculate EMA for a the price of a stock. We want a 22 day EMA which is a common enough time frame for a long EMA.
//The formula for calculating EMA is as follows:
// EMA = Price(t) * k + EMA(y) * (1 – k)
// t = today, y = yesterday, N = number of days in EMA, k = 2/(N+1)
// Use the following steps to calculate a 22 day EMA:
//1) Start by calculating k for the given timeframe. 2 / (22 + 1) = 0,0869
//2) Add the closing prices for the first 22 days together and divide them by 22.
//3) You’re now ready to start getting the first EMA day by taking the following day’s (day 23) closing price multiplied by k, then multiply the previous day’s moving average by (1-k) and add the two.
//4) Do step 3 over and over for each day that follows to get the full range of EMA.
           
                
                
                
                ########## Exponential Moving Average (EMA) #########
                // EMA [today] = (Price [today] x K) + (EMA [yesterday] x (1 – K))
                // N = the length of the EMA
                // K = 2 ÷(N + 1)
                // Price [today] = the current closing price
                // EMA [yesterday] = the previous EMA value
                // EMA [today] = the current EMA value
                $ema = NULL;
                if($count==1){
                    $ema = $currentvalue;
                }
                else{
                    $n      = $count;
                    $k      = (2/($n+1));
                    $price  = $currentvalue;
                    $ema_y  = $this->get_previous_ema($name,$days,$date);
                    $ema = ($price * $k) + ($ema_y * (1 - $k));
                    $ema = round($ema,6);
                }

                if($ema&&$this->db->where(array('stockid'=>$id,'date'=>$date,'days'=>$days,'type'=>'EMA'))
                    ->from('averages')->count_all_results()==0){
                    $time = strtotime($date);
                    $this->db->insert('averages',array(
                    'stockid' => $id,
                    'days'    => $days,
                    'value'   => $ema,
                    'date'    => $date,
                    'time'    => $time,
                    'type'    => 'EMA')); 
                }
                
            }
            
          //  p($ar);
            
            return;
            $time = strtotime($date);
            if($id&&$date&&$days&&$time&&
            $this->db->where(array('stockid'=>$id,'date'=>$date,'days'=>$days))
            ->from('averages')->count_all_results()==0){
                $this->db->order_by('date','desc');
                $this->db->limit($days);             
                $ar = $this->db->
                where(array('stockid'=>$id,'date <='=>$date))
                ->from('data')->get()->result_array();
                $count = count($ar);
                // only calculate if there is enough data
                if($count&&$count==$days){      
                    $sum = 0;
                    foreach ($ar as $item) {
                        $sum+=$item['value'];
                    }
                    // Simple Moving Average (SMA)
                    $sma = round(($sum/$count),6);
                    // Exponential Moving Average (EMA).
                    $multiplier = (2/($count+1));
                    
                   
                   p([$sum,$count,$sma,$multiplier]);
                 //  p($ar);
                    
                }
               
            }
            
        }
        
        
}

/* End of file mstock.php */
/* Location: ./application/models/mstock.php */