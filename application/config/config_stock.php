<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Config_stock CONFIG
 * 
 * Description...
 * 
 * @package config_stock
 * @author leon 
 * @version 0.0.0
 */

// useragents will roulate 
$config['ua'] = array(
    'Mozilla/5.0 (Windows NT 5.0; rv:5.0) Gecko/20100101 Firefox/5.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36'
    . ' (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.15 (KHTML, '
    . 'like Gecko) Chrome/24.0.1295.0 Safari/537.15',
    'Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0',
    'Mozilla/5.0 (X11; OpenBSD amd64; rv:28.0) Gecko/20100101 Firefox/28.0',
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:2.0b7) Gecko/20101111 Firefox/4.0b7',
    'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; '
    . 'Trident/4.0; InfoPath.2; SV1; .NET CLR 2.0.50727; WOW64)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3'
    . ' (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10'
);

// moving average ranges zoals ook in database
$config['ma_ranges'] = array(
    2,3,4,5,10,15,20,25,
    30,35,40,45,50,60,70,80,90,100,
    120,140,160,180,200,300,400);

$config['plot_timespan'] = array(
    //'1d' => 1,
    '1w' => 7,//5
    '2w' => 14,//10
    '1m' => 31, //22
    '2m' => 62, //44
    '3m' => 90,//66
    '6m' => 185,//132
    '1y' => 365,
    '2y' => 365*2,
    '5y' => 365*15);

// hoeveel procent van de trend-tijd kijk je terug om te bepalen of een 
// een trend dalen of stijgend is
$config['trend_direction_lookback'] = 20;

// hoeveel procent uitslag van boven of onder gemiddelde
// bepaalt of een trend een bepaalde richting heeft?

$config['trend_direction_swing'] = 1;

// hoeveel dagen in de dagtabel?
// om 3 maanden in de dagtabel te hebben zijn 75 dagen genoeg
// 75 dgn x 100 stocks x 34 kwartieren = 255.000 records (valt dus mee)
$config['days_in_daytable'] = 75;


// hoeveel cijfers achter de komma
$config['plot_decimals']   = 2;

// hoeveel datumlijnen staan er op de x-as
$config['plot_xaxisdates'] = 6;

// hoeveel meetpunten bevat de grafiek ongeveer
$config['plot_precision']  = 300;


/* End of file config_stock.php */
/* Location: ./application/config/config_stock.php */