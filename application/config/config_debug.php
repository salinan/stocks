<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Config_debug CONFIG
 * 
 * Description...
 * 
 * SETbesectorLOG_THRESHOLD TO 1 !!!!
 * 
 * @package config_debug
 * @author leon 
 * @version 0.0.0
 */

/*
 * the ip adresses that output the debugcode with function p()
 */
$config['debug_ips'] = array('84.27.198.200','213.247.119.90','127.0.0.1');

/*
 * the ip adresses that can act out with the cron actions
 * other ip's cannot access 
 * used by : visitors_get_404()
 */
$config['trusted_ips'] = array(
     $_SERVER['SERVER_ADDR'], // current server
    '84.27.198.200',          // LEON
    '213.247.119.90',         // office
    '95.211.144.108');        // backup server   
/*
 * used by : mailmessage()
 */                                                
$config['debug_mailto'] = 'leonardreinders@hotmail.com'; 
$config['debug_logurl']   = 'http://okaymedia.nl/playground/log/log/setlog/'; 
$config['gen_json_key'] = 'is4k9arigyz27hjiju7yyffjd4iwz6jn'; 

/* End of file config_debug.php */
/* Location: ./application/config/config_debug.php */