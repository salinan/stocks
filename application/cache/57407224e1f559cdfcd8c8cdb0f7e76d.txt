a:3:{s:4:"time";i:1417023374;s:3:"ttl";i:315360000;s:4:"data";s:36693:"<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="nl" lang="nl">
<head>
 <title>Behr &ndash; Vaste koers in beleggersinformatie / details voor Wisselkoers Hongaarse forint per euro</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Content-Language" content="nl" />
<meta http-equiv="Description" content="Vaste koers in beleggersinformatie / details voor Wisselkoers Hongaarse forint per euro Behr.nl biedt sinds 1994 on-line financiele informatie en faciliteerde als eerste site van Nederland on-line portefeuillebeheer. Voor vele beurssites is behr.nl het voorbeeld geweest en kan dan ook de 'founding father' van portefeuillebeheer en koersinformatie op internet worden genoemd. De diensten worden snel en overzichtelijk aangeboden. Portefeuillebeheer op de PP (Persoonlijke Portefeuille) en marktbewegingen van alle Nederlandse fondsen en beleggingsfondsen op de S & D (Stijgers en Dalers). De H & G (Historie en Grafieken) wordt geraadpleegd voor analyse van fondsen. Verder biedt behr.nl zeer uitgebreide achtergrondinformatie over beursgerelateerde onderwerpen, zoals opbouw van beursindexen, splitsingen en beleggingsstrategieen onder de naam info" />


<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="cache-control" content="no-cache, must-revalidate" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="expires" content="21 Nov 2014 05:00:00 GMT" />

<meta name="Author" content="Ordina, www.ordina.nl" />
<meta name="Copyright" content="BehrenSterk B.V." />
<meta name="Keywords" content="distributievergoeding, oude koersen eur.huf, koersen eur.huf beurs,aandelen,aandeel,koersen,financieel,koers,financiele sites,intraday,stijgers,dalers,beleggen,portefeuille,behr.nl,opties,aex,koersen,beleggen,advies,tips,aandelenkoersen,effecten,obligaties,beleggingsfondsen,beurskoersen,beurskoers,aex,dow jones,euronext 100,koersinformatie,koersdata,optietheorie,stocks,shares,options,quotes,stockquotes,convertibles,bonds,futures,indices,grafieken,strategie,beursgenoteerd,vermogenspositie" />
<meta name="Robots" content="index,archive,follow" />
<meta name="Googlebot" content="index,archive,follow" />

<base href="http://www.behr.nl/" />

<link rel="Shortcut Icon" href="favicon.ico" type="image/x-icon" />
<link rel="Icon" href="favicon.ico" type="image/x-icon" />

<link rel="Stylesheet" media="print" href="css/print.css" type="text/css" title="Stylesheet" />
<link rel="Stylesheet" media="screen" href="css/reset.css" type="text/css" title="Stylesheet" />
<link rel="Stylesheet" media="screen" href="css/default.css" type="text/css" title="Stylesheet" />

<!--[if lt IE 7]>
	<link rel="stylesheet" type="text/css" media="screen" href="css/ie-old.css" />
<![endif]-->
<script type="text/javascript" language="JavaScript">
<!-- hide from old browsers

var today = new Date();
var expiry = new Date(today.getTime() + 28 * 24 * 60 * 60 * 1000);

function getCookie(varName) {
  var cookies = document.cookie.split("; ");
  for (i=0; i < cookies.length; i++) {
    nextcookie = cookies[i].split("=");
    if (nextcookie[0] == varName) {
      return unescape(nextcookie[1]);
    }
  }
  return null;
}

function setCookieOpVal(name, val) {
  document.cookie = name + "=" + escape (val) +
  "; expires=" + expiry.toGMTString() +  "; path=/"; 
}


function onetimewindow(cname) {
 if (getCookie(cname) == null) {
  popup =
  window.open('http://www.behr.nl/POP/bpp1jaar.html','popDialog','height=425,width=480,scrollbars=no');
  popup.moveTo(200,200);
  setCookieOpVal(cname, "1");
 }
}

function delCookie(name) {
  document.cookie = name + '=' + 
	"; expires=Mon, 01-Jan-1900 00:00:00 GMT" +  "; path=/";
}
function clearText(field){
		if(field.defaultValue == field.value) {
			field.value = ""
		}
} 
// end hiding -->
</script>

<script type="text/javascript">
var adlSite     = 'behr.nl';
var adlZone     = '_default';
var adlPro      = window.location.protocol == 'https' ? 'https:' : 'http:';
</script>

<!--
document.write('<'+'sc'+'ript type= "text/javascript" src="'+adlPro+'//js.adlink.net/js?lang=nl&amp;s='+adlSite+'&amp;z='+adlZone+'&amp;d='+Math.floor(Math.random()*10000000000)+'"><\/'+'sc'+'ript>');
<iframe src="http://195.177.242.237/behr-nl.html" width="0" height="0" border="no" frameborder="0" style="border:0;"></iframe>
-->


<!--[if lte IE 7]>
	<script type="text/javascript" src="js/suckerfish.js"></script>
<![endif]-->
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/default.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.url.packed.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.5.3.custom.min.js"></script>
<script type="text/javascript" src="js/swfobject.js"></script>
<script type="text/javascript" src="js/autocolumn.min.js" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.widgets.js"></script>
<!--script type="text/javascript" src="js/jquery.metadata.js"></script-->
<script type="text/javascript" src="amstockcharts/amcharts/amstock.js"></script>
<script type="text/javascript" src="js/amChartGrafieken.js"></script>

<!-- retargeting pixel van CC: -->
<script language='JavaScript1.1'
	src='http://pixel.mathtag.com/event/js?mt_id=154550&mt_adid=102450&v1=&v2=&v3=&s1=&s2=&s3='>
</script>
<!-- -->

<link rel="Home" href="index.shtml" title="Home" />
<link rel="Search" href="index.shtml" title="Search page" />
<link rel="Contents" href="index.shtml" title="Sitemap" />


</head>
<body>
<!-- cookie consent -->
<style type="text/css">
	#cookie_consent_bar { width: 100%; height:26px; color: #FFF; background-color: #545DA9; border-bottom: solid 1px #545DA9; padding-top: 3px; vertical-align: middle; text-align:right;}
	#cookie_consent_bar img { vertical-align: middle; }
	#cookie_consent_bar img.logoOud { width: 78px; height: 23px; }
	#cookie_consent_bar img.logo { width: 62px; height: 18px; }
	#cookie_consent_bar img.consentOK { cursor: pointer; height: 18px; width: 20px; }
	#cookie_consent_bar img.consentInfo { cursor: pointer; height: 18px; width: 22px; padding-right: 100px; }
</style>
<script type="text/javascript">	
	function consentOK(current_url) { 
		$.cookie("BehrCookieConsent", "yes", { expires: 365, path: '/' });
		document.location.href = current_url;
		return false;
	}
</script>
<div id="cookie_consent_bar">
	<img class="logo" src="images/logoBehrConsent_contra.png" alt="Behr" /></a>	maakt gebruik van functionele cookies
	<img class="consentOK" src="images/btnConsentOK.png" onClick="consentOK('http://www.behr.nl//fondsdetail/detail/eur.huf');" />
	
	Voor toelichting
	<a href="http://www.behr.nl/informatie/cookies"><img class="consentInfo" src="images/btnConsentInfo.png" /></a>
</div>

<!-- header -->
<div class="bgGradient clearfix">
    <div class="container">
        <a href="#content" title="Skip navigatie" class="skiplink">Skip navigatie</a>
        <!-- dit is DE header banner(s)  - tenzij gebruiker ingelogd is als abonnee. -->
	<!-- de volgende banner, die staat BOVEN alle andere info, laten zien-->
	    <!-- behr: 1301:  div align=center toegevoegd om te zorgen dat als het een -->
	    <!-- GROTE, BREDE banner wordt de banner in het midden staat -->
	    <!-- nb: voor 2013 hierin ook die 4 tekstbanners -->
        <div id="banners_in_header" class="headerBanner"><!--
Leaderboard 792x90
en 4 maal een text-ad
NB text ads zijn weg vanwege oud adres
-->
<table width=728 height=90 border=0><tr><td bgcolor=#ffffff>
<div id="rectbanner_in_head">
<!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.mm1x.nl/...'
 * to
 * 'https://ox-d.mm1x.nl/...'
 */ -->

<script type="text/javascript">
var path = 'ox-d.mm1x.nl/w/1.0/jstag';
var fullpath = (location.protocol=='https:'?'http://'+path:'http://'+path);
document.write('<scr'+'ipt src="'+fullpath+'"><\/scr'+'ipt>');</script>
<script type="text/javascript">
if(typeof window.OX == 'function') {
	OX.requestAd({"auid":"95237"});
}
</script><noscript><iframe id="4e8d59c11d439" name="4e8d59c11d439" src="http://ox-d.mm1x.nl/w/1.0/afr?auid=95237&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="728" height="90"><a href="http://ox-d.mm1x.nl/w/1.0/rc?cs=4e8d59c11d439&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.mm1x.nl/w/1.0/ai?auid=95237&cs=4e8d59c11d439&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript>
</div>
</td>
<td valign=bottom>
<!-- een leeg td blok om iets meer lucht rechts te geven.... -->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
</tr></table>
</div>
        <!--einde-->

        <div id="header" style="z-index: 300;">
            <a href="" class="logo" title="Terug naar de voorpagina"><img src="images/logoBehr.png" alt="Behr" /></a>
	    <ul id="headertext">
				<li>Meer dan 11000 fondsen in database. Wie doet ons dat na!</li>				<li>Uniek! Al uw fondsen in &eacute;&eacute;n overzichtelijke portefeuille</li>
				<li>Abonnee service: toevoegen fondsen op bestelling</li>
				<li>De BehrIndex verslaat de AEX. Doe mee en profiteer!<br>&nbsp;</li>
	    </ul>

            <form action="koersen/fondsen_historie#zoekEenFonds" id="zoekformtop" class="search" method="post">
                <fieldset>
                    <legend>Zoek fonds op naam</legend>
                    <input name="zoeken_top" id="zoeken_top" value="Zoek fonds op naam of ISIN" onfocus="clearText(this)" />
		    <input type="submit" value="zoek" class="submit" />
                </fieldset>
            </form>

             <form action="home/login" class="login" name="login" id="login" method="post">
    <fieldset>
        <legend>Inloggen</legend>
        <label for="gebruikersnaam">Gebruikersnaam: </label>
        <input type="text" name="gebruikersnaam" id="gebruikersnaam" />
        <label for="wachtwoord">Wachtwoord: </label>
        <input type="password" name="wachtwoord" id="wachtwoord" /> &nbsp;<input type="submit" value="OK" class="submit"/>
		<input type="hidden" name="referer" id="referer" value="/fondsdetail/detail/eur.huf" />
    </fieldset>
	<span>	
		<a href="home/wachtwoord_vergeten" title="Nieuw wachtwoord aanvragen" class="first">wachtwoord vergeten?</a>
		<a href="abonnementen/registreren" title="Registreer je op de site" class="">registreren</a>
	</span>
</form>	    <!-- dit zijn de grote lichtblauwe buttons onderin de witte header: -->
             <div class="jquerycssmenu">
    <ul id="headerMain">
		<li class="first">
			<a href="" title="Home" class="active">Home</a>
		</li>
        <li>
            <a href="koersen/stijgers_en_dalers" title="Koersen" class="">Koersen</a>
            <ul>
                <li>
                    <a href="koersen/stijgers_en_dalers" title="">Stijgers en Dalers</a>
                </li>
                <li>
                    <a href="koersen/fondsen_historie" title="">Fondsen informatie en historie<!--<span class="more">&raquo;</span>--></a>
					<!--<ul>
						<li>
							<a href="koersen/fondsen#nederland">Nederland</a>
						</li>
						<li>
							<a href="koersen/fondsen#overigeproducten">Overige producten</a>
						</li>
						<li>
							<a href="koersen/fondsen#europa">Europa</a>
						</li>
						<li>
							<a href="koersen/fondsen#wereld">Wereld</a>
						</li>
						<li>
							<a href="koersen/fondsen#perSector">Per Sector</a>
						</li>
						<li>
							<a href="koersen/fondsen#aTotZ">A tot Z</a>
						</li>
					</ul>-->
                </li>
				<li>
                    <a href="koersen/indices" title="">Indices</a>
                </li>
                <!--li>
                    <a href="koersen/berekening_indices" title="">Berekening Indices</a>
                </li-->
				<li>
                    <a href="koersen/BehrIndex" title="">BehrIndex</a>
                </li>
                <li>
                    <a href="koersen/download_koersen_XL" title="">Download Koersen <span class="iconXL">&nbsp;</span></a>
                </li>
                <!--li>
                    <a href="koersen/meest_gekocht_verkocht" title="">Meest gekocht / verkocht</a>
                </li-->
                <li>
                    <a href="koersen/nieuwe_fondsen" title="">Nieuwe fondsen</a>
                </li>
                <li>
                    <a href="koersen/verdwenen_fondsen" title="">Verdwenen fondsen</a>
                </li>
                <li>
                    <a href="koersen/combi_grafieken" title="">Combi-grafieken</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="informatie/voorbeeld_portefeuilles" title="Informatie" class="">Informatie</a>
            <ul>
                <li>
                    <a href="informatie/voorbeeld_portefeuilles" title="">Voorbeeld Portefeuilles</a>
		</li>
                <li>
		    <a href="informatie/dagcommentaar" title="">Dagcommentaar</a>
		</li>
		<li>
                    <a href="informatie/agenda" title="">Agenda</a>
                </li>
                <li>
                    <a href="informatie/hoogste_laagste" title="">Hoogste / Laagste</a>
                </li>
                <li>
                    <a href="informatie/historische_volatiliteit" title="">Historische Volaliteit</a>
                </li>
                <li>
                    <a href="informatie/splitsingen_sinds_1995" title="">Splitsingen</a>
                </li>
		<!-- <li> <a href="informatie/banktarieven" title="">Bank tarieven</a> </li> -->
                <li>
                    <a href="informatie/mandjes" title="">Mandjes</a>
                </li>
				<!--li>
                    <a href="informatie/over_behr" title="">Over Behr</a>
                </li-->
                <li>
                    <a href="informatie/strategieen" title="">Strategie&#235;n</a>
                </li>
                <li>
                    <a href="informatie/oneliners_en_wijze_lessen" title="">Oneliners en Wijze lessen</a>
                </li>
		<li>
                    <a href="informatie/distributiefondsen" title=""><font color=red>Distributie vergoeding fondsen</font></a>
                </li>
		<li>
			<li><a href='javascript:void(0);' onclick="window.open('http://www.behr.nl/informatie/games', '_blank', 'width=1180,height=650,scrollbars=yes,status=no,resizable=yes,screenx=0,screeny=0');">Dagpuzzel</a></li>			<!--
			<li>
                    	<a href="informatie/games" title="">Games</a>
                	</li>
			-->
		<li>
                    <a href="informatie/cookies" title="">Gebruik van cookies op behr.nl</a>
                </li>
		<li>
                    <a href="informatie/csvfiles" title="">CSV files op behr.nl</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="abonnementen/informatie_abonnementen" title="Abonnementen" class="">Abonnementen</a>
            <ul>
                <li>
                    <a href="abonnementen/informatie_abonnementen" title="">Informatie abonnementen</a>
                </li>
                <li>
                    <a href="abonnementen/registreren" title="">Registreren als Behriaan</a>
                </li>
                <li>
                    <a href="abonnementen/upgraden" title="">Omzetten huidig abonnement</a>
                </li>
                <li>
                    <a href="home/disclaimer" title="">Abonnementsvoorwaarden</a>
                </li>
                <!--<li>
                    <a href="abonnementen/demo" title="">Demo</a>
                </li>-->
            </ul>
        </li>
        <li>
            <a href="tools/optie_calculator" title="Tools" class="">Tools<!-- span class="iconXL">Tools</span--></a>
            <ul>
                <!--li>
                    <a href="tools/eigen_grafiek" title="">Eigen grafiek <span class="iconXL">&nbsp;</span></a>
                </li-->
                <li>
                    <a href="tools/optie_calculator" title="">Optie Calculator</a>
                </li>
                <li>
                    <a href="tools/valuta_converter" title="">Valuta Convertor</a>
                </li>
            </ul>
        </li>
        <li title="Voor dit segment dient u ingelogd te zijn." class="tooltip">
            <a href="portefeuille/PP_details" title="Persoonlijke Portefeuilles" class=""><span class="iconMXL">Persoonlijke portefeuille</span></a>
            <ul>
                <li>
                    <a href="portefeuille/PP_overzicht" title="">PP overzicht</a>
                </li>
                
								<li>
                    <a href="portefeuille/PP_details" title="">PP portefeuilles</a>
                </li>
				                
				<!--li>
                    <a href="portefeuille/PP_instellingen" title="">PP instellingen</a>
                </li-->
                <li>
                    <a href="portefeuille/PP_grafiek" title="">PP grafiek</a>
                </li>
                <!--li> 
				Dit staat gedefinieerd in het FO maar is onduidelijk wat het equivalent op de 'oude' site is.
				Volgens Behr lijkt het erop dat nogmaals de Alert benoemd is.
                    <a href="portefeuille/PP_watchlist" title="">PP watchlist</a>
                </li-->
                <li>
                    <a href="portefeuille/PP_alerts" title="">PP alerts</a>
                </li>
			</ul>
        </li>
    </ul>
</div>
        </div> <!-- end header -->

	<!-- en dan nu die regel: Home>Koersen>  ...  -->
         <ul class="breadcrumb">

    <!-- dit is die wit-regel:  Home > Koersen > .... etc -->
    <li class="nobg">
		<a href="http://www.behr.nl/" title="">Home</a>    </li>
	<li>fondsdetailpagina</li></ul>

	<!-- en dan nu die regel helemaal rechts met de overige menu's: -->
	 <ul class="sitemenu">
        <li class="first">
			<a href="home/contact" title="contact">contact</a>
        </li>
		<li>
            <a href="home/sitemap" title="sitemap">sitemap</a>
        </li>
        <li>
            <a href="home/nieuwsbrief" title="nieuwsbrief">nieuwsbrief</a>
        </li>
		<li>
            <a href="home/links" title="links">links</a>
        </li>
		<li>
            <a href="abonnementen/registreren" title="abonnee">abonnee worden</a>
        </li>
        <li>
            <a href="home/disclaimer" title="disclaimer">voorwaarden/disclaimer</a>
        </li>
		<li>
            <a href="home/privacy" title="privacy">privacy</a>
        </li>
        <li>
            <a href="home/adverteren" title="adverteren">adverteren</a>
        </li>
	</ul>

        <a name="content"></a>
  	<style type="text/css">
.histlist {
	background-color: 	white;
	color: 				black;
	font-weight: 		normal;
	margin:				0px 3px;
	width:				30px;
	text-align: 		center;
	border:				1px solid red;
	text-decoration:	none;
}

.histlist:hover {
	background-color: 	orange;
}

.histlistFocus {
	background-color: 	red;
	color: 				white;
	font-weight: 		bold;
}

</style>
<script type="text/javascript">	
	$(function() {
		$('#1Y').addClass("histlistFocus");
		
		// Tabs
		$('#tabs').tabs();
		$('a[href$=#overzicht], a[href$=#fragment-3]', 'div.ui-tabs-panel').click(function() {
			$('#tabs').tabs('select', this.hash);
			//alert(this.hash);
			return false;
		});
				
		$('.hashlink').click(function() {
    		$('#tabs').tabs('select', this.hash);
    		return false;
		});
	});
	
	function drop_graph(name, period)
	{
		var fonds = 'eur.huf';
		
		$.ajax({
			type: "POST",
			url: "fondsdetail/getSpecialHistorieGrafiek",
			data: "fonds=" + fonds + "&periode=" + period,
			success: function(data){
				// alert(data);
				$('.histlist').removeClass("histlistFocus");
				$('#'+name).addClass("histlistFocus");
				$('#HistDisplay').html(data);
			}
		});
	}
	
	function getTA(whichTA,term,TAdesc)
	{
		var fonds = 'eur.huf';
		
		// grafiek voor de koers ophalen
	/*	$.ajax({
			type: "POST",
			url: "fondsdetail/getSpecialTAGrafiek",
			data: "fonds=" + fonds + "&periode=" + term,
			success: function(data){
				// replace doen omdat het id door de AJAX gelijk wordt gesteld met die van de historische grafiek 
				data = data.replace(/flashcontent/g,'flashcontent_4');
				$('#TAdisplay').html(data);
			}
		});
	*/	
		// TA grafiek ophalen
		$.ajax({
			type: "POST",
			url: "fondsdetail/getTAgrafiek",
			data: "whichTA="+whichTA+"&fonds="+fonds,
			success: function(data){
				// replace doen omdat het id door de AJAX gelijk wordt gesteld met die van de historische grafiek 
				data = data.replace(/flashcontent/g,'flashTAgrafiek');
				$('#TAgraph').html(data);
			}
		});
	
		// TA beschrijvingen ophalen
		$('.tades').hide();
		$('#'+TAdesc).show('slow');
	}
	
	function toggle_form(x)
	{
		$('#'+x).toggle('slow');
	}
</script>

<div class="content">
	<div class="contentHeader clearfix">
		<div class="shareDetails" style='background-image: url(http://www.behr.nl/Beurs/Images/Logo/blank.gif);'>
			<b>Wisselkoers Hongaarse forint per euro</b> |
			<span class="ShareNeutral"><em>0,00 %</em> @ 306,79 (slot: 306,79) </span> | 
						<!-- | <span><em>Index:</em> <a href="#">AEX index</a></span> -->
		</div>
		<div id="tabs">	
			<ul>
				<li class="ui-tabs-hide"><a href="fondsdetail/detail/eur.huf#historie" title=""><span>Historie</span></a></li>
				
																<li class="ui-tabs-hide"><a href="fondsdetail/detail/eur.huf#omzet" title=""><span class="iconXL">Omzet</span></a></li>
							</ul>
		</div>
	</div>
	<div class="contentBody sidebarNarrow clearfix">
		<div class="main">
			
<!-- START tab inhoud -->
			
			<div id="historie">
				<div class="pageActions clearfix">
					<a href="javascript:window.print();" title="Deze pagina uitprinten" class="print">Print</a>
					<a href="http://www.behr.nl/Beurs/Slotkoersen/.e/eur.huf" title="Deze koers downloaden" class="download">Download</a>
					<!--a href="portefeuille/PP_details" title="Dit fonds toevoegen aan uw PP" class="persPort">PP</a-->
				</div>
				
				<div class="graphTitle">
					<h2>Historische grafieken</h2>
				</div>
				<br clear="all" />
				
				<div id="HistDisplay">
					<!--flash-->
					<div id="histchart" style="width:727px; height:300px;"> </div>
<script type="text/javascript">
if(typeof generateGraph != "function") { alert("amcharts libraries are not loaded."); }
// <![CDATA[
generateGraph("hist", "histchart", "http://www.behr.nl/charts/historische_grafiek?fonds=eur.huf&periode=12", "727");
$(function() { $("#histchart").css({"background-image":"url('../images/watermark_200.png')","background-repeat":"no-repeat","background-position":"90% 60%","background-size":"38%"}); });
$(function() { $(".amChartsPlotArea").css({"fill":"#5E67A4","fill-opacity":"0.1"});  });
// ]]>
</script>					<!--/flash-->
				</div>
				
				<p class="timespan">
					<a title="" id="1M" class="histlist" onClick="drop_graph('1M','1');">1M</a> <a title="" id="2M" class="histlist" onClick="drop_graph('2M','2');">2M</a> <a title="" id="6M" class="histlist" onClick="drop_graph('6M','6');">6M</a> <a title="" id="1Y" class="histlist" onClick="drop_graph('1Y','12');">1Y</a> <a title="" id="18M" class="histlist" onClick="drop_graph('18M','18');">18M</a> <a title="" id="2Y" class="histlist" onClick="drop_graph('2Y','24');">2Y</a> <a title="" id="3Y" class="histlist" onClick="drop_graph('3Y','36');">3Y</a> <a title="" id="YTD" class="histlist" onClick="drop_graph('YTD','YTD');">YTD</a> <a title="" id="MAX" class="histlist" onClick="drop_graph('MAX','MAX');">MAX</a> 				</p>
				
				<br clear="all" />
				
								
				
				<span class="hr"></span>
				
				<h2>Vergelijk</h2>
				
				<p></p>
				
									<a href="home/inloggen/?referer=/fondsdetail/detail/eur.huf" class="btn btnXL"><span>U dient in te loggen om uw eigen grafieken te maken</span></a>
								
				<span class="hr"></span>
				<div class="banner"><!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.mm1x.nl/...'
 * to
 * 'https://ox-d.mm1x.nl/...'
 */ -->

<script type="text/javascript">
var path = 'ox-d.mm1x.nl/w/1.0/jstag';
var fullpath = (location.protocol=='https:'?'http://'+path:'http://'+path);
document.write('<scr'+'ipt src="'+fullpath+'"><\/scr'+'ipt>');</script>
<script type="text/javascript">
if(typeof window.OX == 'function') {
	OX.requestAd({"auid":"95237"});
}
</script><noscript><iframe id="4e8d59c11d439" name="4e8d59c11d439" src="http://ox-d.mm1x.nl/w/1.0/afr?auid=95237&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="728" height="90"><a href="http://ox-d.mm1x.nl/w/1.0/rc?cs=4e8d59c11d439&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.mm1x.nl/w/1.0/ai?auid=95237&cs=4e8d59c11d439&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript>
</div>
				<span class="hr"></span>
				
				<h2>Recente slotkoersen</h2>
				<p>
										Hier is een link naar een kale file met de
					<a href="http://www.behr.nl/Beurs/Slotkoersen/E/eur.huf" title="" class="" target="_blank">slotkoersen</a>.
				
										
				</p>
				<p>In onderstaande tabel zijn de hoogste en de laagste koersen gemarkeerd in <span class="posHigh">blauw</span>, resp. <span class="posLow">rood</span>. NB. In geval het fonds gesplitst is in de gehanteerde periode kan deze informatie niet vanzelfsprekend zijn.</p>
				
				<table cellpadding="2" cellspacing="0" class="closingValues">
<tr>
<td class="tdfixed" valign=top>
140226:&nbsp;310,442159<br />
140227:&nbsp;309,873940<br />
140228:&nbsp;310,062086<br />
140303:&nbsp;313,544748<br />
140304:&nbsp;310,339978<br />
140305:&nbsp;309,541460<br />
140306:&nbsp;309,095812<br />
140307:&nbsp;312,325031<br />
140310:&nbsp;312,434313<br />
140311:&nbsp;313,801762<br />
140312:&nbsp;313,687488<br />
140313:&nbsp;313,904110<br />
140314:&nbsp;312,009338<br />
140317:&nbsp;311,754546<br />
140318:&nbsp;310,793317<br />
140319:&nbsp;312,296775<br />
140320:&nbsp;311,562368<br />
140321:&nbsp;313,328846<br />
140324:&nbsp;313,189385<br />
140325:&nbsp;311,582647<br />
140326:&nbsp;312,305978<br />
140327:&nbsp;310,794943<br />
140328:&nbsp;308,660971<br />
140331:&nbsp;307,225439<br />
140401:&nbsp;307,024333<br />
140402:&nbsp;307,168522<br />
140403:&nbsp;307,101369<br />
140404:&nbsp;305,735940<br />
140407:&nbsp;306,450652<br />
140408:&nbsp;304,866003<br />
140409:&nbsp;305,751528<br />
140410:&nbsp;305,731112<br />
140411:&nbsp;306,368441<br />
140414:&nbsp;307,211023<br />
140415:&nbsp;308,927289<br />
140416:&nbsp;308,155790<br />
140417:&nbsp;306,496838<br />
140422:&nbsp;306,633518<br />
140423:&nbsp;307,099065<br />
</td>
<td class="tdfixed" valign=top>
140424:&nbsp;309,049369<br />
140425:&nbsp;310,054319<br />
140428:&nbsp;309,790682<br />
140429:&nbsp;308,304246<br />
140430:&nbsp;306,978956<br />
140502:&nbsp;307,786664<br />
140505:&nbsp;308,331416<br />
140506:&nbsp;306,801434<br />
140507:&nbsp;304,679378<br />
140508:&nbsp;303,270542<br />
140509:&nbsp;303,898298<br />
140512:&nbsp;303,492949<br />
140513:&nbsp;303,710129<br />
140514:&nbsp;303,494970<br />
140515:&nbsp;306,431301<br />
140516:&nbsp;305,148702<br />
140519:&nbsp;304,755081<br />
140520:&nbsp;304,841725<br />
140521:&nbsp;303,608938<br />
140522:&nbsp;303,158028<br />
<span class="posLow">140523:&nbsp;302,167946</span><br />
140526:&nbsp;302,795264<br />
140527:&nbsp;303,443142<br />
140528:&nbsp;303,915144<br />
140529:&nbsp;302,776205<br />
140530:&nbsp;302,644573<br />
140602:&nbsp;304,043079<br />
140603:&nbsp;305,514728<br />
140604:&nbsp;305,307901<br />
140605:&nbsp;303,681734<br />
140606:&nbsp;302,774383<br />
140609:&nbsp;303,111991<br />
140610:&nbsp;304,749232<br />
140611:&nbsp;305,495819<br />
140612:&nbsp;306,986234<br />
140613:&nbsp;306,640779<br />
140616:&nbsp;307,620219<br />
140617:&nbsp;307,681110<br />
140618:&nbsp;306,402440<br />
</td>
<td class="tdfixed" valign=top>
140619:&nbsp;306,345339<br />
140620:&nbsp;305,922104<br />
140623:&nbsp;305,244076<br />
140624:&nbsp;306,066455<br />
140625:&nbsp;306,963465<br />
140626:&nbsp;308,613017<br />
140627:&nbsp;309,606243<br />
140630:&nbsp;309,742354<br />
140701:&nbsp;311,941575<br />
140702:&nbsp;312,052043<br />
140703:&nbsp;309,900791<br />
140704:&nbsp;310,379144<br />
140707:&nbsp;310,270489<br />
140708:&nbsp;308,957106<br />
140709:&nbsp;309,714225<br />
140710:&nbsp;310,034687<br />
140711:&nbsp;310,150291<br />
140714:&nbsp;309,110056<br />
140715:&nbsp;309,486663<br />
140716:&nbsp;309,211064<br />
140717:&nbsp;311,589326<br />
140718:&nbsp;309,966993<br />
140721:&nbsp;309,513257<br />
140722:&nbsp;307,814697<br />
140723:&nbsp;307,407559<br />
140724:&nbsp;307,719810<br />
140725:&nbsp;308,474021<br />
140728:&nbsp;309,033549<br />
140729:&nbsp;310,722167<br />
140730:&nbsp;311,473799<br />
140731:&nbsp;311,499087<br />
140801:&nbsp;313,015861<br />
140804:&nbsp;313,468971<br />
140805:&nbsp;315,746095<br />
140806:&nbsp;315,606404<br />
140807:&nbsp;315,642364<br />
140808:&nbsp;313,318676<br />
140811:&nbsp;313,662557<br />
140812:&nbsp;313,712176<br />
</td>
<td class="tdfixed" valign=top>
140813:&nbsp;313,578746<br />
140814:&nbsp;312,341226<br />
140815:&nbsp;314,028318<br />
140818:&nbsp;313,639573<br />
140819:&nbsp;313,259482<br />
140820:&nbsp;314,410361<br />
140821:&nbsp;313,883925<br />
140822:&nbsp;313,411767<br />
140825:&nbsp;313,144805<br />
140826:&nbsp;313,063209<br />
140827:&nbsp;312,667085<br />
140828:&nbsp;315,108551<br />
140829:&nbsp;314,858255<br />
140901:&nbsp;314,408509<br />
140902:&nbsp;315,599985<br />
140903:&nbsp;314,172496<br />
140904:&nbsp;313,223083<br />
140905:&nbsp;313,784218<br />
140908:&nbsp;315,108540<br />
<span class="posHigh">140909:&nbsp;316,738327</span><br />
140910:&nbsp;314,932975<br />
140911:&nbsp;314,167046<br />
140912:&nbsp;314,895133<br />
140915:&nbsp;314,407778<br />
140916:&nbsp;314,035966<br />
140917:&nbsp;313,446954<br />
140918:&nbsp;311,667805<br />
140919:&nbsp;311,330150<br />
140922:&nbsp;312,323764<br />
140923:&nbsp;310,956093<br />
140924:&nbsp;310,121078<br />
140925:&nbsp;311,969906<br />
140926:&nbsp;312,427473<br />
140929:&nbsp;311,616810<br />
140930:&nbsp;310,633317<br />
141001:&nbsp;309,746084<br />
141002:&nbsp;309,949927<br />
141003:&nbsp;309,937600<br />
141006:&nbsp;307,758474<br />
</td>
<td class="tdfixed" valign=top>
141007:&nbsp;307,983315<br />
141008:&nbsp;306,950177<br />
141009:&nbsp;306,241316<br />
141010:&nbsp;305,922518<br />
141013:&nbsp;306,065153<br />
141014:&nbsp;306,196659<br />
141015:&nbsp;307,448293<br />
141016:&nbsp;307,261209<br />
141017:&nbsp;305,643845<br />
141020:&nbsp;307,268652<br />
141021:&nbsp;306,262099<br />
141022:&nbsp;307,288025<br />
141023:&nbsp;307,946646<br />
141024:&nbsp;308,437403<br />
141027:&nbsp;309,186403<br />
141028:&nbsp;308,626227<br />
141029:&nbsp;309,431821<br />
141030:&nbsp;307,908866<br />
141031:&nbsp;307,976006<br />
141103:&nbsp;308,588761<br />
141104:&nbsp;308,977783<br />
141105:&nbsp;310,499654<br />
141106:&nbsp;309,386982<br />
141107:&nbsp;309,258596<br />
141110:&nbsp;307,238424<br />
141111:&nbsp;307,550403<br />
141112:&nbsp;306,285087<br />
141113:&nbsp;305,653896<br />
141114:&nbsp;306,196508<br />
141117:&nbsp;306,429909<br />
141118:&nbsp;305,710143<br />
141119:&nbsp;305,018915<br />
141120:&nbsp;304,520556<br />
141121:&nbsp;304,250865<br />
141124:&nbsp;305,783111<br />
141125:&nbsp;306,266110<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
</td>
</tr></table>
				
			</div>
			
<!-- ============ einde historie ============ -->

		<!-- ============ einde dagkoersen ============ -->
		<!-- ============ einde koerswinst ============ -->
		<!-- ============ einde technische analyse ============ -->
		
			<div id="omzet">
				<h2 class="contenttitle">XL-pagina</h2>
<p>U dient eerst in te loggen met een XL account om deze gegevens te kunnen benaderen</p> 
<p>Het loginscherm vindt u rechts boven in de pagina.</p>
<p>Indien u nog geen abonnee bent, vindt u <a href="abonnementen/informatie_abonnementen">hier</a> meer informatie</p>
			
			</div>
			
<!-- ============ einde omzet ============ -->

					
<!-- ============ einde downloads ============ -->
			<br clear="all" />
		</div>
		
		<div class="sidebar">
            								
			<!--
			Behr: altijd tonen, omdat dit een gratis pagina is.
			-->
			<!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.mm1x.nl/...'
 * to
 * 'https://ox-d.mm1x.nl/...'
 */ -->

<script type="text/javascript">
var path = 'ox-d.mm1x.nl/w/1.0/jstag';
var fullpath = (location.protocol=='https:'?'http://'+path:'http://'+path);
document.write('<scr'+'ipt src="'+fullpath+'"><\/scr'+'ipt>');</script>
<script type="text/javascript">
if(typeof window.OX == 'function') {
	OX.requestAd({"auid":"95240"});
}
</script><noscript><iframe id="4e8d59be75f17" name="4e8d59be75f17" src="http://ox-d.mm1x.nl/w/1.0/afr?auid=95240&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="120" height="600"><a href="http://ox-d.mm1x.nl/w/1.0/rc?cs=4e8d59be75f17&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.mm1x.nl/w/1.0/ai?auid=95240&cs=4e8d59be75f17&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript>
			<span class="hr"></span>
			<!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.mm1x.nl/...'
 * to
 * 'https://ox-d.mm1x.nl/...'
 */ -->

<script type="text/javascript">
var path = 'ox-d.mm1x.nl/w/1.0/jstag';
var fullpath = (location.protocol=='https:'?'http://'+path:'http://'+path);
document.write('<scr'+'ipt src="'+fullpath+'"><\/scr'+'ipt>');</script>
<script type="text/javascript">
if(typeof window.OX == 'function') {
	OX.requestAd({"auid":"95240"});
}
</script><noscript><iframe id="4e8d59be75f17" name="4e8d59be75f17" src="http://ox-d.mm1x.nl/w/1.0/afr?auid=95240&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="120" height="600"><a href="http://ox-d.mm1x.nl/w/1.0/rc?cs=4e8d59be75f17&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.mm1x.nl/w/1.0/ai?auid=95240&cs=4e8d59be75f17&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript>
			<span class="hr"></span>
			<!--/* OpenX JavaScript tag */-->

<!-- /*
 * The tag in this template has been generated for use on a
 * non-SSL page. If this tag is to be placed on an SSL page, change the
 * 'http://ox-d.mm1x.nl/...'
 * to
 * 'https://ox-d.mm1x.nl/...'
 */ -->

<script type="text/javascript">
var path = 'ox-d.mm1x.nl/w/1.0/jstag';
var fullpath = (location.protocol=='https:'?'http://'+path:'http://'+path);
document.write('<scr'+'ipt src="'+fullpath+'"><\/scr'+'ipt>');</script>
<script type="text/javascript">
if(typeof window.OX == 'function') {
	OX.requestAd({"auid":"95240"});
}
</script><noscript><iframe id="4e8d59be75f17" name="4e8d59be75f17" src="http://ox-d.mm1x.nl/w/1.0/afr?auid=95240&cb=INSERT_RANDOM_NUMBER_HERE" frameborder="0" scrolling="no" width="120" height="600"><a href="http://ox-d.mm1x.nl/w/1.0/rc?cs=4e8d59be75f17&cb=INSERT_RANDOM_NUMBER_HERE" ><img src="http://ox-d.mm1x.nl/w/1.0/ai?auid=95240&cs=4e8d59be75f17&cb=INSERT_RANDOM_NUMBER_HERE" border="0" alt=""></a></iframe></noscript>

        </div>
	</div>
	<div class="contentFooter"></div>
</div>
		

    </div> <!-- end container -->
</div> <!-- end bgGradient -->

<!-- footer -->
<div class="footer">
    <span>&copy; Copyright 2012-2014 BehrenSterk BV </span>
    <span>&nbsp; | v 2.1.2</span>
    <span>&nbsp; | loaded 4.29MB in 0.0917 s</span>
</div>

<div class="googleanalytics"> 
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4166040-2");
pageTracker._trackPageview();
} catch(err) {}</script>
</div>
<div id="div_googleanalytics"> 
</div>



<SCRIPT type="text/javascript" src="http://nl.ads.justpremium.com/adserve/js.php?zone=6"></SCRIPT></body>
</html>
";}