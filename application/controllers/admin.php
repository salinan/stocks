<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Admin CONTROLLER
 * 
 * Description...
 * 
 * @package admin
 * @author leon 
 * @version 0.0.0
 */

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
                $this->load->driver('cache', array('adapter' => 'file'));
                $this->load->model('mstock');
                 //$this->mstock->cascade_secondary_datatables();
	}
        
        public function index(){
            
           // echo md5('0lRpCUSP6Z');
            
            $this->load->view('main_open');
            $this->load->view('page_main');
            $this->load->view('main_close');
        }
        
        // get data --> ajax
        public function get_data(){
            debug_off();
            $function  = $this->input->post('function');
            $stockid   = $this->input->post('stockid');
            $stockname = $this->input->post('stockname');
            $timespan  = $this->input->post('timespan');
            $sma       = $this->input->post('sma');
            $timespans = config_item('plot_timespan');
            $days      = isset($timespans[$timespan])?$timespans[$timespan]:7;
            $dateright = date('Y-m-d');
            $dateleft  = dateminusdays($days,$dateright);
            if(!$stockid){$stockid=1;}
            switch($function) {
                case 'get_data':
                echo $this->mstock->get_stockdata($stockid,$stockname,$dateleft,$dateright,$sma);
                break;
            }
        }
        
        public function test(){
//           debug();
//  
//            $ar = $this->mstock->get_trend_direction(1,15);
//            $ar = $this->mstock->get_trend_direction(1,30);
//            $ar = $this->mstock->get_trend_direction(1,60);
//            $ar = $this->mstock->get_trend_direction(1,120);

            $data = array();
            $data['stocks'] = $this->mstock->get_all_stocks();
            $data['ranges'] = config_item('ma_ranges');
            $data['spans']  = config_item('plot_timespan');
            $data['crumbs'] = $this->uri->segment_array();
            $this->load->view('main_open');
            $this->load->view('page_test',$data);
            $this->load->view('main_close');
        }

        
        public function cron($always=NULL){
            // 9.15 uur tot en met 17.15 uur.elk uur een update
            $h = date('H');          
            $w = date('w');
            // buiten deze tijden zijn alerts zinloos
            if($h<9||$h>17){return;}
            if($w==0||$w==6){return;}// za zo
            $data = array();
            $html = sendrequest('http://www.beurs.nl/koersen/aex/p1');
            $ar = get_nodes($html,array('td','td/a/@href'),"//table[@id='quoteTable']/tr");
            unset($ar[0]);
            foreach ($ar as $item) {
                $item = $item[0] ;
                $id = strtolower(preg_replace('/ /','-',$item[0]));
                $alert_laag = $this->mstock->get_alertvalue("l_$id",$always);
                $alert_hoog = $this->mstock->get_alertvalue("h_$id",$always);
                $data['stocks'][] = array(
                    'groep'=> 'aex',
                    'id'   => $id,
                    'name' => $item[0],
                    'alert_laag' => $alert_laag,
                    'alert_hoog' => $alert_hoog,
                    'prijs'=> $item[2],
                    'bied' => $item[5],
                    'laat' => $item[6],
                    'volume' => $item[8],
                );
            }
            $html2 = sendrequest('http://www.beurs.nl/koersen/amx/p1');
            $ar2 = get_nodes($html2,array('td','td/a/@href'),"//table[@id='quoteTable']/tr");
            unset($ar2[0]);
            foreach ($ar2 as $item) {
                $item = $item[0] ;
                $id = strtolower(preg_replace('/ /','-',$item[0]));
                $alert_laag = $this->mstock->get_alertvalue("l_$id",$always);
                $alert_hoog = $this->mstock->get_alertvalue("h_$id",$always);
                
                $data['stocks'][] = array(
                    'groep'=> 'amx',
                    'id'   => $id,
                    'name' => $item[0],
                    'alert_laag' => $alert_laag,
                    'alert_hoog' => $alert_hoog,
                    'prijs'=> $item[2],
                    'bied' => $item[5],
                    'laat' => $item[6],
                    'volume' => $item[8],
                );
            }
            $count=0;
            $alerts = array('dalers'=>array(),'stijgers'=>array()); 
            foreach ($data['stocks'] as $item) {
              extract($item);
              $xlaag = preg_replace('/,/','.',$alert_laag);
              $xhoog = preg_replace('/,/','.',$alert_hoog);
              $xprijs = preg_replace('/,/','.',$prijs);
              
              if($xlaag&&$xprijs<$xlaag){
               $pct = calc_pctdiff($alert_laag,$prijs);
               $alerts['dalers'][] = "<strong>$name</strong> $alert_laag - $prijs ($pct%)";
               $count++;
               $this->mstock->set_alertsend("l_$id");
              }
              if($xhoog&&$xprijs>$xhoog){
               $pct = calc_pctdiff($alert_hoog,$prijs);
               $alerts['stijgers'][] = "<strong>$name</strong> $alert_hoog - $prijs (+$pct%)";
               $count++;
               $this->mstock->set_alertsend("h_$id");
              }
            }
            // send the alert mail
            if($count){
              sendjson($alerts,
              'http://okaymedia.nl/playground/stocks/sendalert','stockalert');
            } 
        }
        
        
        
        public function update_stocks(){
            $function = $this->input->post('function');
            switch($function) {
                case 'yahooticker':
                    $value = trim($this->input->post('value'));
                    $id    = $this->input->post('id');
                    $this->mstock->update_yahooticker($id,$value);
                break;
                case 'name':
                    $value = trim($this->input->post('value'));
                    $id    = $this->input->post('id');
                    $this->mstock->update_stockname($id,$value);
                break;
                case 'beursnl':
                    $value = trim($this->input->post('value'));
                    $id    = $this->input->post('id');
                    $this->mstock->update_beursnlticker($id,$value);
                break;
                case 'deletestock':
                    $id    = $this->input->post('id');
                    $this->mstock->delete_stock($id);
                    $this->mstock->cascade_secondary_datatables();
                break;
                case 'setactive':
                    $status = trim($this->input->post('status'));
                    $id    = $this->input->post('id');
                    $this->mstock->update_stockstatus($id,$status);
                break;
                case 'newticker':
                    $ticker = trim($this->input->post('ticker'));
                    // new ticker?
                    $count = $this->mstock->test_yahooticker($ticker);
                    if($count){
                        echo 'Stock is already added'; return;
                    }
                    echo $this->mstock->get_yahooticker($ticker);
                    // get isin for all stocks that did not have one yet
                    $this->mstock->stock_get_isin_extrainfo();
                    echo ' - '. $this->mstock->yahoo_historical_data_new_stock();
                    $this->mstock->cascade_secondary_datatables();
                break;
            }
        }
        
        public function stocks(){
            $data = array(
                'stocks' => $this->mstock->get_all_stocks(),
                'crumbs'=>$this->uri->segment_array()
            );
            $this->load->view('main_open');
            $this->load->view('page_stocks',$data);
            $this->load->view('main_close');
            
        }
        
        // moet nog gemaakt !!!!!!!!!!!!!!!!!!
         public function events(){
            $data = array(
                'stocks' => $this->mstock->get_all_stocks()
            );
            $this->load->view('main_open');
            $this->load->view('page_stocks',$data);
            $this->load->view('main_close');
            
        }
        
        // ajax
        public function update_alerts(){
            $alerts = $this->input->post('alerts');
            $ar=json_decode($alerts,TRUE);
            foreach ($ar as $key => $value) {
                $this->mstock->set_alertvalue($key,$value);
            }
        }
        
        // controller
        public function alerts(){
            $h = date('H');          
            $w = date('w');
            $buitenbeurs = FALSE;
            if($h<9||$h>17){$buitenbeurs=TRUE;}
            if($w==0||$w==6){$buitenbeurs=TRUE;}// za zo
            $data = array();
           
            $html = sendrequest('http://www.beurs.nl/koersen/aex/p1');
            //debug();
            $ar = get_nodes($html,array('td','td/a/@href'),"//table[@id='quoteTable']/tr");
            unset($ar[0]);
            foreach ($ar as $item) {
                $item = $item[0] ;
                $id = strtolower(preg_replace('/ /','-',$item[0]));
                $ar_laag = $this->mstock->get_alert("l_$id");
                $ar_hoog = $this->mstock->get_alert("h_$id");  
                $data['stocks'][] = array(
                    'groep'=> 'aex',
                    'id'   => $id,
                    'name' => $item[0],
                    'alert_laag' =>$ar_laag['value'],
                    'alert_hoog' =>  $ar_hoog['value'],
                    'send_laag' =>$ar_laag['send']=='Yes'?'sent':'',
                    'send_hoog' =>$ar_hoog['send']=='Yes'?'sent':'',
                    'prijs'=> $item[2],
                    'bied' => $item[5],
                    'laat' => $item[6],
                    'volume' => $item[8],
                );
            }
            $html2 = sendrequest('http://www.beurs.nl/koersen/amx/p1');

            $ar2 = get_nodes($html2,array('td','td/a/@href'),"//table[@id='quoteTable']/tr");
            unset($ar2[0]);
             foreach ($ar2 as $item) {
                $item = $item[0] ;
                $id = strtolower(preg_replace('/ /','-',$item[0]));
                $ar_laag = $this->mstock->get_alert("l_$id");
                $ar_hoog = $this->mstock->get_alert("h_$id");
                $data['stocks'][] = array(
                    'groep'=> 'amx',
                    'id'   => $id,
                    'name' => $item[0],
                    'alert_laag' => $ar_laag['value'],
                    'alert_hoog' => $ar_hoog['value'],
                    'send_laag' =>$ar_laag['send']=='Yes'?'sent':'',
                    'send_hoog' =>$ar_hoog['send']=='Yes'?'sent':'',
                    'prijs'=> $item[2],
                    'bied' => $item[5],
                    'laat' => $item[6],
                    'volume' => $item[8],
                );
            }
            
            //p($data);
            
            $this->load->view('main_open');
            $this->load->view('page_alerts',$data);
            $this->load->view('main_close');
            
            
        }
        
        
        
	public function aex()
	{
            
            $data = array();
            $html = get_page('http://www.beurs.nl/koersen/aex/p1');
            $ar = get_nodes($html,array('td','td/a/@href'),"//table[@id='quoteTable']/tr");
            $data = array('stocks'=>array());
            foreach ($ar as $item) {
                if($item[1]){ 
                    $url2 = "http://www.beurs.nl".$item[1];
                    $html2 = get_page($url2);
                    $ar2 = get_nodes($html2,array('//img[@id="quoteChart"]/@src'));
                    $name = $item[0][0];
                                 
                    $data['stocks'][] = array(
                        'name' => $name,
                        'src'  => $ar2[0][0],
                        'class' => strtolower(preg_replace('/ /','-',$item[0][0])),
                        'info' => ''
                    );     
                }
            }
            
            $html = get_page('http://www.beurs.nl/koersen/amx/p1');
            $ar = get_nodes($html,array('td','td/a/@href'),"//table[@id='quoteTable']/tr");
            
            foreach ($ar as $item) {
                if($item[1]){ 
                    $url2 = "http://www.beurs.nl".$item[1];
                    $html2 = get_page($url2);
                    $ar2 = get_nodes($html2,array('//img[@id="quoteChart"]/@src'));
                    $name = $item[0][0];
                    
                  
                   
                    $data['stocks'][] = array(
                        'name' => $name,
                        'src'  => $ar2[0][0],
                        'class' => strtolower(preg_replace('/ /','-',$item[0][0])),
                        'info' => ''
                    );     
                }
            }   
            
            $this->load->view('main_open');
            $this->load->view('page',$data);
            $this->load->view('main_close');
         

	}
        
       
        

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */