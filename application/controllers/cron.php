<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Cron CONTROLLER
 * 
 * Description...
 * 
 * @package cron
 * @author leon 
 * @version 0.0.0
 */

class Cron extends CI_Controller {

	public function __construct() {
		parent::__construct();
                $this->load->driver('cache', array('adapter' => 'file'));
                $this->load->model('mstock');
                
	}
	
	public function index()
	{
		debug();
                $this->mstock->yahoo_get_historical_data('AALB.AS','2014-12-15');
	}
        
	public function aex_slot()
	{
            $this->mstock->yahoo_get_quote_data();
	}

        
	public function aex_per_uur()
	{
            if(amsterdam_open()){
                 $this->mstock->yahoo_get_quote_data_interval(); 
            }
	}
        

        

        
        
        
        
        

}

/* End of file cron.php */
/* Location: ./application/controllers/cron.php */