<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Stock_helper HELPER
 * 
 * Description...
 * 
 * @package stock_helper
 * @author leon 
 * @version 0.0.0
 */

function get_assoc($ar){
    $ar2 = array();
    foreach ($ar as $item) {
        foreach ($item as $value) {
             $ar2[]=$value;break;
        } 
    }
    return $ar2;
}

// werkt alleen in deze tijdzone
// 9.15 uur tot en met 17.15 uur
function amsterdam_open(){
    $h = date('H');          
    $w = date('w');
    $m = intval(date('i'));
    if($h<9||($h>17&&$m>30)){return FALSE;}
    if($w==0||$w==6){return FALSE;}// za zo
    return TRUE;
}

// tel zoveel dagen terug
function dateminusdays($days=10,$date=NULL){
    if(!$date){$date = date('Y-m-d');}
    $time = strtotime($date);
    $time -= 60*60*24*$days;
    return date('Y-m-d',$time);
}

// alle dagen in een periode behalve de weekenden
function workingdays($days=10,$date=NULL,$weekdays=array(1,2,3,4,5)){
    if(!$date){$date = date('Y-m-d');}
    $time = strtotime($date);
    $counter = 0;
    $dates=array($date);
    while ($counter<=$days) {
        $time -= 60*60*24;
        if(in_array(date('w',$time),$weekdays)){
            $dates[] = date('Y-m-d',$time);
        }
        $counter++;
    }
    return $dates;
}




function trailzero($str=''){
    return (($str-0)<10)?"0$str":$str;
}

function calc_pctdiff($val1=NULL,$val2=NULL){
    $val1= preg_replace('/,/','.',$val1);
    $val2= preg_replace('/,/','.',$val2);
    $val1 = $val1-0;
    $val2 = $val2-0;
    $diff = $val2-$val1;
    $pct = ($diff/$val1)*100;
    $pct = round($pct,2);
    return $pct;
}

// get a remote htmlpage only by use of proxy
// optionally use a proxy username:password (set in config)
function sendrequest($url=NULL,$binary=FALSE,$manualproxy=NULL,$timeout=20,$postdata=NULL,$referer=NULL,$proxyuserpwd=NULL){
    if(!$url){return;}
    if(!$timeout){$timeout=20;}
    $ua        = config_item('ua');
    $options   = array(
                     CURLOPT_USERAGENT       => $ua[array_rand($ua)],
                     CURLOPT_HEADER          => FALSE,
                     CURLOPT_RETURNTRANSFER  => TRUE,
                     CURLOPT_FOLLOWLOCATION  => TRUE,
                     CURLOPT_TIMEOUT         => $timeout);
   
    if($manualproxy){
        $proxy = $manualproxy;
        $proxy = trim($proxy);
        $proxy = preg_replace('/\n/','',$proxy);
        if(!$proxy||!preg_match('/^[\d\.]+\:\d+$/',$proxy)){
            echo 'incorrect proxy :'.$proxy ; return;
        }
        $options[CURLOPT_HTTPPROXYTUNNEL]=FALSE;
        $options[CURLOPT_PROXY]=$proxy;//ip:port
        $options[CURLOPT_URL]=$url; 
        if($proxyuserpwd){  
           $options[CURLOPT_PROXYUSERPWD]=$proxyuserpwd;
        } 
    }
    else{
        $options[CURLOPT_URL]=$url;
    }
    if($referer){
        $options[CURLOPT_REFERER]=$referer;
    }
    // downloading images et all...
    if($binary===TRUE){
         $options[CURLOPT_BINARYTRANSFER]=TRUE;
    }
    // sending a form ..duh
    if(is_array($postdata)&&count($postdata)){
        $data = array();
        foreach ($postdata as $key=>$value){
            $data[]="$key=$value";
        }
        $options[CURLOPT_POST]=1;
        $options[CURLOPT_POSTFIELDS]=implode('&',$data);
    }
    $ch = curl_init();
    curl_setopt_array($ch, $options);
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   // p($httpcode);
    curl_close($ch);
    if($httpcode=='0'){$httpcode = 'error';}
    return ($httpcode == 200)?$response:$httpcode;
}

function uncache_page($url=NULL){
    if(!$url){return;}
    $cache_id = md5($url).'.txt';
    $ci =& get_instance();
    if($ci->cache->get($cache_id)){
        $ci->cache->delete($cache_id);
    }
}

// get a page from cache of real live from the INTENET!!!!
function get_page($url=NULL,$manualproxy=NULL,$timeout=20,$postdata=NULL,$referer=NULL,$proxyuserpwd=NULL){
     //try to find the cache : 
    $cache_id = md5($url).'.txt';
    $ci =& get_instance();
    $text = $ci->cache->get($cache_id);
    if($text){return $text;}
    $data = sendrequest($url,FALSE,$manualproxy,$timeout,$postdata,$referer,$proxyuserpwd);
    if($data){ 
         $ci->cache->save($cache_id, $data, 60*60*24*365*10);
         usleep(500000);
    }
    return $data;
}

// HTML_TO_XPATH : FROM HTMLSTRING CREATE A DOMDOCUMENT AND RETURN AN XPATHOBJECT
function html_to_xpath($html){
        $doc = new DOMDocument();
        $doc->preserveWhiteSpace = FALSE;
        @$doc->loadHTML($html);
        return new DOMXPath($doc);
}

// TIDY_DOMNODEVALUE : CLEANUP A HTMLSTRING FOR USAGE
function tidy_domnodevalue($nodevalue=''){
       //$nodevalue=xutf8_decode($nodevalue);// shit!!
       $nodevalue=preg_replace("@[\\0\n\r\t\x0B]+@",' ',$nodevalue);
       $nodevalue=preg_replace('/ +/',' ',$nodevalue);
       return trim($nodevalue);
}
// GETNODES : CREATE XMLDOM AND TRAVERSE WITH XPATH
function get_nodes(
                $html="", 		// HTML GEEN URL
                $xqueries=array(), 	// ASSOCIATIEVE ARRAY 
                $context="/", 	        // PUNT (.) GEEFT RELATIEVE NODE AAN
                $nodeid=""	        // id om direct een blok te selecteren
        ){
        $xpath = html_to_xpath($html);
        $contextitems = $xpath->query($context);
        $alleblokken = array();
        // het blok wat wordt doorlopen...
        $parentkey=false;
        foreach ($contextitems as $contextitem){
                $alletypes=array();
                foreach ($xqueries as $xqueryname => $xquery){
                        $nodelist = $xpath->query($xquery,$contextitem);    
                        $nodevalue = NULL;
                        // alleen de eerst waarde wordt uitgelezen 
                        $tmp = array();
                        foreach ($nodelist as $node){
                                $nodevalue = $node->nodeValue;
                                $tmp[]     = tidy_domnodevalue($nodevalue);
                        }
                        // er is meer als 1 resultaat
                        if(count($tmp)>1){
                               $alletypes[$xqueryname]=$tmp;
                        }
                        else{
                            if(isset($nodevalue)){
                               $alletypes[$xqueryname]=$nodevalue;
                               if($nodeid&&$nodeid==$xqueryname){$parentkey=$nodevalue;}
                            }
                            else{ $alletypes[$xqueryname]=NULL;}
                        }
                }
                // voeg dit blok info toe
                if($parentkey){$alleblokken[$parentkey]=$alletypes;}
                else{$alleblokken[]=$alletypes;}
        }
        unset($html,$xpath,$nodelist,$xquery,$url,$queries,$tmp);
        return $alleblokken;
}


/* End of file stock_helper.php */
/* Location: ./application/helpers/stock_helper.php */