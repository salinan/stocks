<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');



// non trusted ip's will get 404 page here
function visitors_get_404($mail=FALSE){
    if(!in_array(ip(), config_item('trusted_ips'))){
           if($mail){mailmessage('no access : '.__METHOD__);}
           show_404();
    }      
}

// WALKER FOR ARRAY ASSO
function _asso_walker(&$arr,$key,$id){
      $arr = $arr[$id];
}
// ARRAY_ASSO_VALUES : GET THE VALUES FROM AN ASSOCIATIVE ARRAY FROM ONE KEY
// WHEN NEEDED CHUNK THEM IN SUBARRAYS (FOR LONG SQL WHERE-IN STATEMENTS)
function array_asso_values($id,$arr,$chunks=NULL){
     array_walk($arr, '_asso_walker',$id);
     return ($chunks!=NULL)? array_chunk($arr,$chunks):$arr;
}

function sort_httpcode_desc($item1,$item2){
    if(!isset($item1['httpcode']) || !isset($item2['httpcode'])) return 0;
    if($item1['httpcode'] == $item2['httpcode']) return 0;
    return ($item1['httpcode'] < $item2['httpcode']) ? 1 : -1;
}




// use the classes of the debugger as functions
function p($value='',$key=NULL,$line='...'){
    $ci =& get_instance();
    $ci->debug->p($value,$key,$line); 
}

function q($value='LAST QUERY'){
    $ci =& get_instance();
    $ci->debug->p($ci->db->last_query(),$value);
}
function mem($value='memory_get_usage'){
    $ci =& get_instance();
    $ci->debug->p(round(memory_get_usage()/1024/1024, 2),$value);
}
// only for debugger ip's
function is_debug(){
   return in_array($_SERVER['REMOTE_ADDR'],config_item('debug_ips'))?TRUE:FALSE;
}




// put the debuggerclass on for some ip's
function debug($value=NULL,$key=NULL){
    $ci =& get_instance();
    $ci->debug->on();
    $ci->debug->force=FALSE;
    $ci->benchmark->mark('debug_start');
    if($value){
        p($value,$key);
    }
}
// put the debuggerclass on for everyone
function debug_force($value=NULL,$key=NULL){
    $ci =& get_instance();
    $ci->debug->on();
    $ci->debug->force=TRUE;
    $ci->benchmark->mark('debug_start');
    if($value){
        p($value,$key);
    }
}

// put the debuggerclass on
function debug_off(){
    $ci =& get_instance();
    $ci->debug->off();
}

// what is my ip ??
function ip(){
    return $_SERVER['REMOTE_ADDR'];
}

function tables(){
    $ci =& get_instance();    
    debug($ci->db->list_tables());
  
}
function fields($table){
    $ci =& get_instance(); 
    debug($ci->db->list_fields($table));
}
function table($table=NULL,$rows='25',$filter=NULL){
    $ci =& get_instance(); 
    $ar = array('tables'=>$ci->db->list_tables());
    if($table) {
        if($filter){$this->db->where($filter);}
        $ar = array_merge($ar,array(
        'table'       => $table,
        'rows'   => $ci->db->from($table)->count_all_results(),
        'fields' => $ci->db->list_fields($table),
        'data'   => $ci->db->from($table)
        ->limit($rows)->get()->result_array()));
        $ar['rows']=count($ar['data']).'/'.$ar['rows'];
    } 
    debug($ar);
    return $ar;
}

// mail a debuggingmessage
function mailmessage($msg = ''){
    logline('message sent on : ' .date('Y-m-d H:i:s'));
    logline('server : '.$_SERVER['SERVER_NAME'].' @ '.$_SERVER['SERVER_ADDR']);
    logline('by remote server : '.ip().'<hr>');
    logline($msg);
    $message = implode('<br>',$GLOBALS['loglines']);
    $headers= "From: The Test Messenger <testmessage@messageland.com>\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    mail(config_item('debug_mailto'),"Testmessage @ ".$_SERVER['HTTP_HOST'],$message,$headers);
}

function logline($line='',$reset=FALSE){
    if(!isset($GLOBALS['loglines'])||$reset==TRUE){
        $GLOBALS['loglines'] = array();
    }
    if($line){
        if(is_array($line)){
           $GLOBALS['loglines']= array_merge($GLOBALS['loglines'],$line);
        }
        else{
             $GLOBALS['loglines'][]=$line;
        }
    }
}

function sendjson($data=NULL,$url,$key=NULL){
    // requires MY_Encrypt lib ,gen_json_key
    $data = json_encode($data);
    $ci =& get_instance(); 
    $ci->load->library('encrypt');
    $data = $ci->encrypt->encode($data,($key!==NULL?$key:config_item('gen_json_key')));
    $data = urlencode($data);
    $ch = curl_init();
    curl_setopt_array($ch, array(
                     CURLOPT_URL             => $url,
                     CURLOPT_USERAGENT       => 'Mozilla/5.0 (Windows NT 5.0; rv:5.0) Gecko/20100101 Firefox/5.0',
                     CURLOPT_HEADER          => FALSE,
                     CURLOPT_RETURNTRANSFER  => TRUE,
                     CURLOPT_SSL_VERIFYHOST  => FALSE,
                     CURLOPT_FOLLOWLOCATION  => FALSE,
                     CURLOPT_TIMEOUT         => 15,
                     CURLOPT_POST            => TRUE,
                     CURLOPT_POSTFIELDS      => "json=$data"));
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}


function getjson($key=NULL){
    // requires MY_Encrypt lib
    $ci =& get_instance();
    $data = $ci->input->post('json');
    $ci->load->library('encrypt');
    $data = $ci->encrypt->decode($data,($key!==NULL?$key:config_item('gen_json_key')));
    return json_decode($data,TRUE);
}
// send a debugging message
function remotedebug($subject='',$body=''){
    $data = sendjson(array(
        'servername' => $_SERVER['SERVER_NAME'],
        'serverip'   => $_SERVER['SERVER_ADDR'],
        'remoteip'   => ip(),
        'servertime' => date('Y-m-d H:i:s'),
        'subject'    => $subject,
        'body'       => $body
    ),config_item('debug_logurl'));     
}









/* End of file debug_helper.php */
/* Location: ./application/helpers/debug_helper.php */