        <header>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                   <div class="row">
                       
                       <div class="span12 ">         
                        <ul class="nav navbar-nav nav-stocks">
                            <li>
                                <ul class="breadcrumb list-inline">
                                    <?php 
                                    if(count($crumbs)):
                                    $path = array();?> 
                                     <li><a href="<?php echo base_url() ;?>">Home</a><span class="divider">/</span></li>
                                    <?php foreach ($crumbs as $key => $crumb):
                                    $path[] = $crumb;?> 
                                    <?php if($crumb==end($crumbs)):?>
                                    <li class="active"><?php echo $crumb ;?></li>
                                    <?php else:?>
                                     <li><a href="<?php echo base_url(implode('/',$path)) ;?>"><?php
                                     echo $crumb ;?></a> <span class="divider">/</span></li>
                                    <?php endif;?>
                                    <?php endforeach;endif?>
                                </ul> 
                            </li>
                            <li>
                                 <div class="btn-group">
                                    <?php foreach ($ranges as $range):?>
                                      <button class="btn btn-small group-range" value="<?php 
                                      echo $range ;?>"><?php echo $range ;?></button>
                                     <?php endforeach;?>
                                 </div>
                                <div class="btn-group">
                                    <?php foreach ($spans as $name => $span):?>
                                      <button class="btn btn-small group-timespan" value="<?php 
                                      echo $name ;?>"><?php echo $name ;?></button>
                                     <?php endforeach;?>
                                 </div>
                            </li>
                        </ul>            
                       </div>
                     </div>
                </div>
            </div>
        </div>
        </header>
        <div class="container">
             <div class="row"> 
                <div class="span3">
                   
                    <div id="select-stock">
                        <ul class="unstyled">
                        <?php foreach ($stocks as $stock):
                        extract($stock); 
                        $cls1 = (!$status) ? 'icon-eye-close':'icon-eye-open';
                        $cls2 = (!$status) ? 'off':'on';
                        ?>
                            <li class="<?php 
                            echo $cls2 ;?>"><i id="id1_<?php echo $id ;?>" class="<?php
                            echo $cls1 ;?>"></i> 
                            <a id="id2_<?php echo $id ;?>" href="#" class="<?php
                            echo $cls2 ;?>"><?php echo $name ;?></a></li>
                        <?php endforeach;?>
                        </ul>
                    </div>             
                </div>
                <div class="span9">
                     <div id="graph"></div>
                </div>
            </div>
           
           
            <footer>
                 
            </footer>
        </div>