        <header>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                   <div class="row">
                       <div class="span12 btns">
                           <button id="toon-actieve" type="button" 
                                   class="btn">Toon Alleen Actieve</button>
                          
                       </div>
                     </div>
                </div>
            </div>
        </div>
        </header>
        <div class="container">
             <div class="row"> 
                <div class="span12">
                    <table class="stocks table table-bordered table-condensed">
                     <thead>
                     <tr>
                         <th>
                             Type
                         </th>  
                         <th>
                             Status
                         </th>  
                         <th>
                             Naam
                         </th>  
                         <th>
                             Beurs.nl
                         </th>  
                         <th>
                             Yahoo Ticker
                         </th>  
                           
                    </tr>
                   </thead>
                   <tbody>
                  <?php 
                  foreach ($stocks as $stock):
                  extract($stock);
                 
                  ?>
                   
                    <tr>
                        <td>
                           <?php echo $type ;?>
                        </td>
                        <td>
                           <button type="button" class="btn btn-small<?php
                           if($status=='Actief'){echo ' btn-success';} ?>"><?php echo $status ;?></button> 
                        </td>
                        <td>
                            <input id="sn_<?php echo $id ;?>" value="<?php 
                           echo $name ;?>" type="text" class="input-medium sn">
                        </td>
                        <td>
                           <input id="sb_<?php echo $id ;?>" value="<?php 
                           echo $beursnl ;?>" type="text" class="input-medium sb">
                        </td>
                        <td>
                           <input id="sy_<?php echo $id ;?>" value="<?php 
                           echo $yahoo ;?>" type="text" class="input-small sy">
                        </td>
                         <td>
                           <button type="button" class="btn btn-small">Delete</button> 
                        </td>
                        
                            
                    </tr>
                    
                    
                  <?php endforeach;?>
                    </tbody>
                    </table>
                </div>
            </div>
           
           
            <footer>
                 
            </footer>
        </div>