        <header>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                   <div class="row">
                       <div class="span12 btns">
                           <button id="update-alerts" type="button" 
                                   class="btn">Update Alerts</button>
                           <button id="clear-alerts" type="button" 
                                   class="btn">Clear Alerts</button>
                           <button id="set-alerts-m1" type="button" 
                                   class="btn">Lage alerts -1%</button>
                           <button id="set-alerts-m2" type="button" 
                                   class="btn">Lage alerts -2%</button>
                           <button id="set-alerts-h1" type="button" 
                                   class="btn">Hoge alerts +1%</button>
                           <button id="set-alerts-h2" type="button" 
                                   class="btn">Hoge alerts +2%</button>
                       </div>
                     </div>
                </div>
            </div>
        </div>
        </header>
        <div class="container">
             <div class="row"> 
                <div class="span12">
                    <table class="stocks table table-bordered table-condensed">
                     <thead>
                     <tr>
                         <th>
                             Groep
                         </th>  
                         <th>
                             Naam
                         </th>  
                         <th>
                             Laag Alert
                         </th>  
                         <th>
                             Hoog Alert
                         </th>  
                         <th>
                             Prijs
                         </th>  
                         <th>
                             Bied
                         </th>  
                         <th>
                             Laat
                         </th>  
                         <th>
                             Volume
                         </th>  
                            
                    </tr>
                   </thead>
                   <tbody>
                  <?php 
                  foreach ($stocks as $stock):
                  extract($stock);
                 
                  ?>
                   
                    <tr>
                        <td>
                           <?php echo $groep ;?>
                        </td>
                        <td>
                           <?php echo $name ;?>
                        </td>
                        <td>
                           <input type="hidden" id="p_<?php 
                           echo $id ;?>" value="<?php 
                           echo $prijs ;?>" class="prijs">
                           <input id="l_<?php 
                           echo $id ;?>" value="<?php 
                           echo $alert_laag ;?>" type="text" class="laag alert input-small <?php 
                           echo $send_laag ;?>">
                        </td>
                        <td>
                           <input id="h_<?php 
                           echo $id ;?>" value="<?php 
                           echo $alert_hoog ;?>" type="text" class="hoog alert input-small <?php 
                           echo $send_hoog ;?>">
                        </td>
                        <td>
                           <?php echo $prijs ;?>
                        </td>
                        <td>
                           <?php echo $bied ;?>
                        </td>
                        <td>
                           <?php echo $laat ;?>
                        </td>
                        <td>
                           <?php echo $volume ;?>
                        </td>
                            
                    </tr>
                    
                    
                  <?php endforeach;?>
                    </tbody>
                    </table>
                </div>
            </div>
           
           
            <footer>
                 
            </footer>
        </div>