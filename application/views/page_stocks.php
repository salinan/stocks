        <header>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                   <div class="row">
                       <div class="span12 ">         
                        <ul class="nav navbar-nav">
                             <li>
                                <ul class="breadcrumb list-inline">
                                    <?php 
                                    if(count($crumbs)):
                                    $path = array();?> 
                                     <li><a href="<?php echo base_url() ;?>">Home</a><span class="divider">/</span></li>
                                    <?php foreach ($crumbs as $key => $crumb):
                                    $path[] = $crumb;?> 
                                    <?php if($crumb==end($crumbs)):?>
                                    <li class="active"><?php echo $crumb ;?></li>
                                    <?php else:?>
                                     <li><a href="<?php echo base_url(implode('/',$path)) ;?>"><?php
                                     echo $crumb ;?></a> <span class="divider">/</span></li>
                                    <?php endif;?>
                                    <?php endforeach;endif?>
                                </ul> 
                            </li>
                            <li>
                                <form class="form-inline add-stock">
                                    <input id="new-stock-ticker" value="" type="text" class="input-small">
                                    <button id="addstock" type="button" 
                                            class="btn btn-success btn-small">
                                        Add Yahoo Ticker + Add Historical Data</button>
                                    <img id="loading" src ="<?php echo base_url('img/indicator.gif') ;?>" />
                                </form>
                            </li>
                        </ul>            
                       </div>
                     </div>
                </div>
            </div>
        </div>
        </header>
        <div class="container">
             
             <div class="row"> 
                <div class="span12">
                    
                     
                    
                    <table class="stocks table table-bordered table-condensed">
                     <thead>
                     <tr>
                         <th>
                            Type
                         </th>  
                         <th>
                            Status
                         </th>  
                         <th>
                            Naam
                         </th>  
                         <th>
                            Yahoo Ticker
                         </th>  
                         <th>
                            Exchange
                         </th> 
                         <th>
                            Currency
                         </th> 
                         <th>
                             
                         </th> 
                    </tr>
                   </thead>
                   <tbody>   
                  <?php 
                  foreach ($stocks as $stock):
                  extract($stock);
                 
                  ?>
                   
                    <tr>
                        <td>
                           <?php echo $type ;?>
                        </td>
                        <td>
                           <button type="button" id="ba_<?php echo $id ;?>" class="ba btn btn-small<?php
                           if($status=='Actief'){echo ' btn-success';} ?>">Status</button> 
                        </td>
                        <td>
                            <input id="sn_<?php echo $id ;?>" value="<?php 
                           echo $name ;?>" type="text" class="input-large sn">
                        </td>
                       
                        <td>
                           <input id="sy_<?php echo $id ;?>" value="<?php 
                           echo $yahoo ;?>" type="text" class="input-small sy">
                           <button title="Yahoo finance - <?php echo $name ;?>" value="<?php echo $yahoo ;?>" type="button" class="btn btn-small yho-out"><i class="icon-share"></i></button> 
                        </td>
                        <td>
                          <?php echo $exchange ;?>
                        </td>
                        <td>
                          <?php echo $currency ;?>
                        </td> 
                         <td>
                           <button id="delstock_<?php 
                           echo $id ;?>" type="button" class="btn btn-small delstock">Delete</button> 
                        </td>
                        
                            
                    </tr>
                    
                    
                  <?php endforeach;?>
                    </tbody>
                    </table>
                </div>
            </div>
           
           
            <footer>
                 
            </footer>
        </div>