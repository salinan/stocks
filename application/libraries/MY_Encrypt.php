<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * My_encrypt LIBRARY
 * 
 * Description...
 * 
 * @package MY_Encrypt
 * @author leon 
 * @version 0.0.0
 */

class MY_Encrypt extends CI_Encrypt
{
    public function __construct()
    {
        parent::__construct();

        //Pretend Mcrypt doesn't exist no matter what
        $this->_mcrypt_exists = FALSE;

    }
}

/* End of file MY_Encrypt.php */
/* Location: ./application/libraries/MY_Encrypt.php */