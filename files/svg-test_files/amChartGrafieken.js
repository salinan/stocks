/**
 * amCharts grafieken: HTML5 versie
 * deze scripts worden gebruikt om de grafieken te genereren.
 *
 */

	/* globale variabelen */
	var chart;
	var graph;
	var chartData = [];
	var chartTitle = [];
	var chartHeader = [];
	var config;
	
	/* generates specific graphs for each configuration */
	function generateGraph(config, div, DataUrl,divWidth) {		
	//	alert(config+"\n"+div+"\n"+DataUrl);
		/* need to reset the data arrays else it keeps the previous data sets */
		chart;
		graph;
		chartData = [];
		chartTitle = [];
		chartHeader = [];
		config;
		if(DataUrl.match(/\bdagkoers/)) {
			intradayData(DataUrl,config);
			intradayGraph(div,divWidth,config);
		}
		else if(DataUrl.match(/\bmeerdere_dagkoersen_grafiek/)) {
			MultidayData(DataUrl,config);
			MultidayGraph(div,divWidth,config);
		}
		else if(DataUrl.match(/\bhistorische_grafiek/)) {
			combiData(DataUrl,config);
			histGraph(div,config);
		}
		else if( DataUrl.match(/\bvoorbeeldportefeuille_grafiek/) || DataUrl.match(/\bcombi_grafieken/) || DataUrl.match(/\bportefeuille_grafieken/) ) {
			combiData(DataUrl,config);
			combiGraph(div,config);
		}
		else if (config == 'ADX9') {
			ADX9_Data(DataUrl,config);
			ADX9_Graph(div,config);
		}
		else if (config.match(/\bEVG/)) {
			EVG_Data(DataUrl,config);
			EVG_Graph(div,config);
		}
		else if (config.match(/\bRSI/i)) {
			RSI_Data(DataUrl,config);
			RSI_Graph(div,config);
		}
		else if (config.match(/\bMACD/)) {
			MACD_Data(DataUrl,config);
			MACD_Graph(div,config);
		}
		else if (config == 'BB20') {
			BB20_Data(DataUrl,config);
			BB20_Graph(div,config);
		}
		else if (config == 'GMvV13-21') {
			GMvV_Data(DataUrl,config);
			GMvV_Graph(div,config);
		}
		else if (config == 'STOCH5') {
			STOCH5_Data(DataUrl,config);
			STOCH5_Graph(div,config);
		}
		else if (config == 'WMpctR7') {
			WMR7_Data(DataUrl,config);
			WMR7_Graph(div,config);
		}
		else if (config.match(/\bCCI/)) {
			CCI_Data(DataUrl,config);
			CCI_Graph(div,config);
		}
		else if (config == 'volume') {
			Volume_Data(DataUrl,config);
			Volume_Graph(div,config);
		}
		else if (config == 'candlestick') {
			Candlestick_Data(DataUrl,config);
			Candlestick_Graph(div,config);
		}
		else if (config == 'financebar') {
			OHLC_Data(DataUrl,config);
			OHLC_Graph(div,config);
		}
		else {
		//	alert(config+"\n"+div+"\n"+DataUrl);
			$('#'+div).html("ERROR: cannot write HTML5 <u>"+config+"</u> graph.");
			$('#'+div).css({"color":"red","font-weight":"bold","height":"100px"});
		}
	}
	
	/**
	 * Data retrieval section
	 * the following functions will retrieve data for each configuration
	 */
	function intradayData(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				chartTitle[config] = $(xml).find('chart').find("title:first").text() ;
				chartHeader[config] = $(xml).find('header').find("text").text() ;
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						var items = line.split(',');
						var tijd = intradayDate(items[0]);
						var huidige_koers = items[1];
						var vorige_koers = items[2];
						
						if(!huidige_koers) {
							dataSet.push({date: tijd, close: vorige_koers});
						} else {
							dataSet.push({date: tijd, koers: huidige_koers, close: vorige_koers});
						}
						
					});
					dataSet.shift(); // eerste entry geeft undefined dus verwijderen.
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	} 
	
	function MultidayData(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				chartTitle[config] = $(xml).find('chart').find("title:first").text() ;
				chartHeader[config] = $(xml).find('header').find("text").text() ;
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						var items = line.split(',');
						var date = MultidayDate(items[0]);
						var koers = items[1];
						var hoog = items[2];
						var laag = items[3];
						dataSet.push({ date: date, koers: koers, hoog: hoog, laag: laag });
						
					});
					dataSet.shift(); // eerste entry geeft undefined dus verwijderen.
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function combiData(DataUrl,config) {
		var dataSet = [];
		var ct = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('data_set').each(function(i) {
					chartTitle.push($(this).find("short").text());
					
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					var ds = [];
					$.each(lines, function(lineNo, line) {
						var items = line.split(',');
						var date = rsiDate(items[0]);
						var koers = items[1];
						ds.push({ date:date, koers:koers});
					});
				//	ds.shift(); // eerste entry geeft undefined dus verwijderen.
					ds.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData.push(ds);
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(ajaxOptions);
			//	alert(thrownError);
			}
		});
	}
	
	function ADX9_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, adx, minDI, plusDI, 20, 40 (last two are guides!!)
						// 20130516,21.846,8.725,46.856,20,40
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var adx = items[1];
						var minDI = items[2];
						var plusDI = items[3];
						dataSet.push({date: datum, adx: adx, mindi: minDI, plusdi: plusDI});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function EVG_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, evg, slot
						// 20130516,17.6344,18.08
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var evg = items[1];
						var slot = items[2];
						dataSet.push({date: datum, evg: evg, slot: slot});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function RSI_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					
					$.each(lines, function(lineNo, line) {
						var items = line.split(',');
						var datum = rsiDate(items[0]);
						var close = items[1];
						dataSet.push({date: datum, close: close});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function MACD_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, macd, evg, 0 (laatste is een guide!)
						// 20130516,0.2708,0.1893,0
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var macd = items[1];
						var evg = items[2];
						dataSet.push({date: datum, macd: macd, evg: evg});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function BB20_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, slot, vg, onder, boven
						// 20130516,18.08,17.4577,16.4309,18.4846
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var slot = items[1];
						var vg = items[2];
						var onder = items[3];
						var boven = items[4];
						dataSet.push({date:datum, slot:slot, vg:vg, onder:onder, boven:boven});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function GMvV_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, gmv, 100 (laatste is een guide!)
						// 20130516,0.2708,0.1893,0
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var gmv = items[1];
						dataSet.push({date: datum, gmv: gmv});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function STOCH5_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, x, k, d, 20, 80 (last two are guides!!)
						// 20130516,22.535,45.517,53.463,20,80
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var x = items[1];
						var k = items[2];
						var d = items[3];
						dataSet.push({date:datum, x:x, k:k, d:d});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function WMR7_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, williams, -20, -80 (last two are guides!!)
						// 20130516,-47.826,-20,-80
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var williams = items[1];
						dataSet.push({date:datum, williams:williams});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function CCI_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, cci, 100, -100 (last two are guides!!)
						// 20121217,209.218,-100,100
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var cci = items[1];
						dataSet.push({date:datum, cci:cci});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function Volume_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, volume
						// 20121204,      104066
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var volume = items[1];
						dataSet.push({date:datum, volume:volume});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function Candlestick_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, low, high, open, close, volume
						// 20121204,14.50,14.68,14.52,14.55,104066
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var low = items[1];
						var high = items[2];
						var open = items[3];
						var close = items[4];
						var volume = items[5];
						dataSet.push({date:datum, low:low, high:high, open:open, close:close, volume:volume});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	function OHLC_Data(DataUrl,config) {
		var dataSet = [];
		$.ajax({
			type : "GET",
			url : DataUrl,
			dataType : "xml",
			async: false,
			success : function(xml) {
				$(xml).find('csv').each(function() {
					var data = $(this).find("data").text();
					var lines = data.split('\n');
					$.each(lines, function(lineNo, line) {
						// columns: date, low, high, open, close, volume
						// 20121204,4.42,4.49,4.42,4.47,4840539
						var items = line.split(',');
						var datum = TAdate(items[0]);
						var low = items[1];
						var high = items[2];
						var open = items[3];
						var close = items[4];
						var volume = items[5];
						dataSet.push({date:datum, low:low, high:high, open:open, close:close, volume:volume});
					});
					dataSet.pop(); // laatste entry geeft undefined dus verwijderen.
					chartData[config] = dataSet;
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
			//	alert(xhr.status);
			//	alert(thrownError);
			}
		});
	}
	
	/**
	 * Graph building section
	 * the following functions will create the graph for each configuration
	 */
	function intradayGraph(div,divWidth,config) {
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.categoryField = "date";
		chart.zoomOutButton = {
			backgroundColor: '#000000',
			backgroundAlpha: 0.15
		};
	//	chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.addTitle(chartTitle[config], 10, "#0040FF", 1, 1);
		chart.addLabel(divWidth-5, 30, chartHeader[config], "right", 8, "#000", 0, 1, 0, "");
		chart.fontSize = 8;
		
		// AXES
		// category
		if(divWidth>100) { var gC = 16; var fS = 6; } else { var gC = 8; var fS = 8; }
		var categoryAxis = chart.categoryAxis;
		categoryAxis.autoGridCount = false;
		categoryAxis.parseDates = true;
		categoryAxis.minPeriod = "ss";
		categoryAxis.gridCount = gC;
		categoryAxis.gridAlpha = 0.5;
		categoryAxis.fontSize = fS;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = true;
		valueAxis.position = "left";
		chart.addValueAxis(valueAxis);
		
		// GRAPH		            
		graph = new AmCharts.AmGraph();
		graph.title = "Huidige koers";
		graph.lineColor = "#FF0000";
		graph.lineThickness = 1;
		graph.valueField = "koers";
		graph.connect = false;
		chart.addGraph(graph);
		
		graph2 = new AmCharts.AmGraph();
		graph2.title = "Slotkoers";
		graph2.lineColor = "#0000FF";
		graph2.lineThickness = 1;
		graph2.dashLength = 4;
		graph2.valueField = "close";
		graph2.connect = false;
		graph2.balloonText = "slot: [[value]]";
		chart.addGraph(graph2);
		
		// CURSOR		
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "JJ:NN";
		chart.addChartCursor(chartCursor);
				
		// WRITE
		chart.write(div);
	}
	
	function MultidayGraph(div,divWidth,config) {
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config].reverse();
	//	chart.dataProvider = chartData[config];
	//	chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.zoomOutButton = {
			backgroundColor: '#000000',
			backgroundAlpha: 0.15
		};
	//	chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.addTitle(chartTitle[config], 10, "#0040FF", 1, 1);
		chart.addLabel(divWidth-5, 30, chartHeader[config], "right", 10, "#000", 0, 1, 0, "");
		chart.fontSize = 8;
		
		// AXES
		// category
		if(divWidth>100) { var gC = 32; var fS = 6; } else { var gC = 24; var fS = 8; }
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true;
		categoryAxis.equalSpacing = true;
		categoryAxis.minPeriod = "mm";
		categoryAxis.dashLength = 1;
		categoryAxis.autoGridCount = false;
		categoryAxis.gridCount = gC;
		categoryAxis.gridAlpha = 0.5;
		categoryAxis.fontSize = fS;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = true;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph = new AmCharts.AmGraph();
		graph.title = "Koers";
		graph.lineColor = "#FF0000";
		graph.lineThickness = 1;
		graph.valueField = "koers";
		graph.connect = false;
	//	graph.balloonText = "[[value]] @ [[date]]";
		chart.addGraph(graph);
		
		graph2 = new AmCharts.AmGraph();
		graph2.title = "Max.";
		graph2.lineColor = "#0000FF";
		graph2.lineThickness = 1;
		graph2.dashLength = 4;
		graph2.valueField = "hoog";
		graph2.connect = false;
		chart.addGraph(graph2);
		
		graph3 = new AmCharts.AmGraph();
		graph3.title = "Min.";
		graph3.lineColor = "#0000FF";
		graph3.lineThickness = 1;
		graph3.dashLength = 4;
		graph3.valueField = "laag";
		graph3.connect = false;
		chart.addGraph(graph3);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "MMM DD, YYYY @ JJ:NN";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
	//	var legend = new AmCharts.AmLegend();
	//	chart.addLegend(legend);
				
		// WRITE
		chart.write(div);
	}
	 
    function histGraph(div,config) {                
		var chart = new AmCharts.AmStockChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.fontSize = 8;
		
		// DATASETS //////////////////////////////////////////
		// create data sets first
		var size = chartData.length;
		for(i=0;i<size;i++) {
			var dataSet = new AmCharts.DataSet();
			dataSet.title = chartTitle[i];
			dataSet.fieldMappings = [{fromField: "koers",toField: "koers"}];
			dataSet.dataProvider = chartData[i];
			dataSet.categoryField = "date";
			if(i>0) { dataSet.compared = true; }
			chart.dataSets.push(dataSet);
		}
		
		// PANELS ///////////////////////////////////////////                                                  
		// first stock panel
		var stockPanel = new AmCharts.StockPanel();
		stockPanel.showCategoryAxis = true;
		stockPanel.title = "Koersen";
		stockPanel.percentHeight = 100;
		stockPanel.recalculateToPercents = "never";
		
		// graph of first stock panel
		var graph = new AmCharts.StockGraph();
		graph.valueField = "koers";
		graph.comparable = true;
		graph.compareField = "koers";
		stockPanel.addStockGraph(graph);
		stockPanel.stockLegend = new AmCharts.StockLegend();
		
		// set panels to the chart
		chart.panels = [stockPanel];
		
		// OTHER SETTINGS ////////////////////////////////////
		var ChartScrollbarSettings = new AmCharts.ChartScrollbarSettings();
		ChartScrollbarSettings.graph = graph;
	//	ChartScrollbarSettings.usePeriod = "DD";
		chart.chartScrollbarSettings = ChartScrollbarSettings;
		
		var categoryAxesSettings = new AmCharts.CategoryAxesSettings();
    	categoryAxesSettings.minPeriod = "DD";
		categoryAxesSettings.startOnAxis = false;
    	chart.categoryAxesSettings = categoryAxesSettings;
    	
		// DATA SET SELECTOR
		if(size > 3) {
			var dataSetSelector = new AmCharts.DataSetSelector();
			dataSetSelector.position = "left";
			chart.dataSetSelector = dataSetSelector;
		}
		
		chart.write(div);
	}
	
    function combiGraph(div,config) {                
		var chart = new AmCharts.AmStockChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.fontSize = 8;
		
		// DATASETS //////////////////////////////////////////
		// create data sets first
		var size = chartData.length;
		for(i=0;i<size;i++) {
			var dataSet = new AmCharts.DataSet();
			var long_name = chartTitle[i].replace(/&nbsp;/g, " ");
			long_name = long_name.replace(/&amp;/g, "&");
			long_name = long_name.replace(/&ccedil;/g, "c");
			long_name = long_name.replace(/&oslash;/g, "o");
			long_name = long_name.replace(/&euro;/g, "euro");
			long_name = long_name.replace(/&acute;/g, " ");
			long_name = long_name.replace(/&aacute;/g, "a");
			long_name = long_name.replace(/&eacute;/g, "e");
			long_name = long_name.replace(/&egrave;/g, "e");
			long_name = long_name.replace(/&atilde;/g, "a");
			long_name = long_name.replace(/&auml;/g, "ae");
			long_name = long_name.replace(/&euml;/g, "e");
			long_name = long_name.replace(/&ouml;/g, "oe");
			long_name = long_name.replace(/&Ouml;/g, "O");
			long_name = long_name.replace(/&uuml;/g, "ue");
			dataSet.title = long_name;
			dataSet.fieldMappings = [{fromField: "koers",toField: "koers"}];
			dataSet.dataProvider = chartData[i];
			dataSet.categoryField = "date";
			if(i>0) { dataSet.compared = true; }
			chart.dataSets.push(dataSet);
		}
		
		// PANELS ///////////////////////////////////////////                                                  
		// first stock panel
		var stockPanel = new AmCharts.StockPanel();
		stockPanel.showCategoryAxis = true;
		stockPanel.title = "Koersen";
		stockPanel.percentHeight = 100;
		stockPanel.recalculateToPercents = "never";
		
		// graph of first stock panel
		var graph = new AmCharts.StockGraph();
		graph.valueField = "koers";
		graph.comparable = true;
		graph.compareField = "koers";
		stockPanel.addStockGraph(graph);
		stockPanel.stockLegend = new AmCharts.StockLegend();
		    	
		// set panels to the chart
		chart.panels = [stockPanel];
		
		// OTHER SETTINGS ////////////////////////////////////
		var categoryAxesSettings = new AmCharts.CategoryAxesSettings();
		categoryAxesSettings.groupToPeriods = ["DD", "WW", "MM"];
		chart.categoryAxesSettings = categoryAxesSettings;


		var sbsettings = new AmCharts.ChartScrollbarSettings();
		sbsettings.graph = graph;
		sbsettings.usePeriod = "WW";
		chart.chartScrollbarSettings = sbsettings;
			
		// DATA SET SELECTOR
		if(size > 3) {
			var dataSetSelector = new AmCharts.DataSetSelector();
			dataSetSelector.position = "left";
			chart.dataSetSelector = dataSetSelector;
		}
		
		chart.write(div);
	}
    
	function ADX9_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = false;
	//	valueAxis.minimum = 0;
	//	valueAxis.maximum = 100;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph1 = new AmCharts.AmGraph();
		graph1.title = "ADX";
	//	graph1.lineColor = "#FF0000";
		graph1.lineThickness = 1;
		graph1.valueField = "adx";
		graph1.connect = false;
		chart.addGraph(graph1);
		
		graph2 = new AmCharts.AmGraph();
		graph2.title = "minDI";
	//	graph2.lineColor = "#0000FF";
		graph2.lineThickness = 1;
		graph2.valueField = "mindi";
		graph2.connect = false;
		chart.addGraph(graph2);
		
		graph3 = new AmCharts.AmGraph();
		graph3.title = "plusDI";
	//	graph3.lineColor = "#FF0000";
		graph3.lineThickness = 1;
		graph3.valueField = "plusdi";
		graph3.connect = false;
		chart.addGraph(graph3);
		
		// GUIDES are vertical (can also be horizontal) lines (or areas) marking some event.
		var guide1 = new AmCharts.Guide();
		guide1.value = "20";
		guide1.lineColor = "#0000FF";
		guide1.lineAlpha = 1;
		guide1.dashLength = 2;
		guide1.inside = true;
		guide1.labelRotation = 0;
		guide1.label = "20";
		valueAxis.addGuide(guide1);
		
		var guide2 = new AmCharts.Guide();
		guide2.value = "40";
		guide2.lineColor = "#0000FF";
		guide2.lineAlpha = 1;
		guide2.dashLength = 2;
		guide2.inside = true;
		guide2.labelRotation = 0;
		guide2.label = "40";
		valueAxis.addGuide(guide2);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
		var legend = new AmCharts.AmLegend();
		chart.addLegend(legend);
	
		// WRITE
		chart.write(div);
	}
	
	function EVG_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = false;
	//	valueAxis.minimum = 0;
	//	valueAxis.maximum = 100;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph1 = new AmCharts.AmGraph();
		graph1.title = "EVG";
	//	graph1.lineColor = "#FF0000";
		graph1.lineThickness = 1;
		graph1.valueField = "evg";
		graph1.connect = false;
		chart.addGraph(graph1);
		
		graph2 = new AmCharts.AmGraph();
		graph2.title = "Slot";
	//	graph2.lineColor = "#0000FF";
		graph2.lineThickness = 1;
		graph2.valueField = "slot";
		graph2.connect = false;
		chart.addGraph(graph2);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
		var legend = new AmCharts.AmLegend();
		chart.addLegend(legend);
	
		// WRITE
		chart.write(div);
	}
	
	function RSI_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config].reverse();
		chart.marginLeft = 10;
		chart.categoryField = "date";
		
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
        // AXES
        // category
        var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
        categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
        categoryAxis.gridAlpha = 0;
		
        // value
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.axisAlpha = 0;
        valueAxis.inside = false;
        valueAxis.minimum = 0;
        valueAxis.maximum = 100;
        chart.addValueAxis(valueAxis);
		
        // GRAPH                
        graph = new AmCharts.AmGraph();
        graph.lineColor = "#FF0000";
        graph.lineThickness = 1;
        graph.valueField = "close";
        chart.addGraph(graph);
        
		// GUIDES are vertical (can also be horizontal) lines (or areas) marking some event.
		// first guide
		var guide1 = new AmCharts.Guide();
		guide1.value = "35";
		guide1.lineColor = "#0000FF";
		guide1.lineAlpha = 1;
		guide1.dashLength = 2;
		guide1.inside = true;
		guide1.labelRotation = 0;
		guide1.label = "35%";
		valueAxis.addGuide(guide1);
        
        // second guide
		var guide2 = new AmCharts.Guide();
		guide2.value = "50";
		guide2.lineColor = "#0000FF";
		guide2.lineAlpha = 1;
		guide2.dashLength = 2;
		guide2.inside = true;
		guide2.labelRotation = 0;
		guide2.label = "50%";
		valueAxis.addGuide(guide2);
		
		// second guide
		var guide3 = new AmCharts.Guide();
		guide3.value = "65";
		guide3.lineColor = "#0000FF";
		guide3.lineAlpha = 1;
		guide3.dashLength = 2;
		guide3.inside = true;
		guide3.labelRotation = 0;
		guide3.label = "65%";
		valueAxis.addGuide(guide3);
		
		// CURSOR
        var chartCursor = new AmCharts.ChartCursor();
        chartCursor.cursorAlpha = 0;
        chartCursor.cursorPosition = "mouse";
        chartCursor.categoryBalloonDateFormat = "DD MMM";
        chart.addChartCursor(chartCursor);
        
        // WRITE
        chart.write(div);
    }
    
	function MACD_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = false;
	//	valueAxis.minimum = 0;
	//	valueAxis.maximum = 100;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph1 = new AmCharts.AmGraph();
		graph1.title = "MACD";
	//	graph1.lineColor = "#FF0000";
		graph1.lineThickness = 1;
		graph1.valueField = "macd";
		graph1.connect = false;
		chart.addGraph(graph1);
		
		graph2 = new AmCharts.AmGraph();
		graph2.title = "EVG";
	//	graph2.lineColor = "#0000FF";
		graph2.lineThickness = 1;
		graph2.valueField = "evg";
		graph2.connect = false;
		chart.addGraph(graph2);
		
		// GUIDES are vertical (can also be horizontal) lines (or areas) marking some event.
		var guide1 = new AmCharts.Guide();
		guide1.value = "0";
		guide1.lineColor = "#0000FF";
		guide1.lineAlpha = 1;
		guide1.lineThickness = 2;
		guide1.dashLength = 2;
		guide1.inside = true;
		guide1.labelRotation = 0;
	//	guide1.label = "20";
		valueAxis.addGuide(guide1);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
		var legend = new AmCharts.AmLegend();
		chart.addLegend(legend);
	
		// WRITE
		chart.write(div);
	}
	
	function BB20_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = false;
	//	valueAxis.minimum = 0;
	//	valueAxis.maximum = 100;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph1 = new AmCharts.AmGraph();
		graph1.title = "Slot";
	//	graph1.lineColor = "#FF0000";
		graph1.lineThickness = 1;
		graph1.valueField = "slot";
		graph1.connect = false;
		chart.addGraph(graph1);
		
		graph2 = new AmCharts.AmGraph();
		graph2.title = "VG";
	//	graph2.lineColor = "#0000FF";
		graph2.lineThickness = 1;
		graph2.valueField = "vg";
		graph2.connect = false;
		chart.addGraph(graph2);
		
		graph3 = new AmCharts.AmGraph();
		graph3.title = "Onder";
	//	graph3.lineColor = "#FF0000";
		graph3.lineThickness = 1;
		graph3.valueField = "onder";
		graph3.connect = false;
		chart.addGraph(graph3);
		
		graph4 = new AmCharts.AmGraph();
		graph4.title = "Boven";
	//	graph4.lineColor = "#FF0000";
		graph4.lineThickness = 1;
		graph4.valueField = "boven";
		graph4.connect = false;
		chart.addGraph(graph4);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
		var legend = new AmCharts.AmLegend();
		chart.addLegend(legend);
	
		// WRITE
		chart.write(div);
	}
	
	function GMvV_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = false;
	//	valueAxis.minimum = 0;
	//	valueAxis.maximum = 100;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph1 = new AmCharts.AmGraph();
		graph1.title = "GMV";
	//	graph1.lineColor = "#FF0000";
		graph1.lineThickness = 1;
		graph1.valueField = "gmv";
		graph1.connect = false;
		chart.addGraph(graph1);
		
		// GUIDES are vertical (can also be horizontal) lines (or areas) marking some event.
		var guide1 = new AmCharts.Guide();
		guide1.value = "100";
		guide1.lineColor = "#0000FF";
		guide1.lineAlpha = 1;
		guide1.lineThickness = 2;
		guide1.dashLength = 2;
		guide1.inside = true;
		guide1.labelRotation = 0;
	//	guide1.label = "20";
		valueAxis.addGuide(guide1);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
	//	var legend = new AmCharts.AmLegend();
	//	chart.addLegend(legend);
	
		// WRITE
		chart.write(div);
	}
	
	function STOCH5_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = false;
	//	valueAxis.minimum = 0;
	//	valueAxis.maximum = 100;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph1 = new AmCharts.AmGraph();
		graph1.title = "X";
	//	graph1.lineColor = "#FF0000";
		graph1.lineThickness = 1;
		graph1.valueField = "x";
		graph1.connect = false;
		chart.addGraph(graph1);
		
		graph2 = new AmCharts.AmGraph();
		graph2.title = "K";
	//	graph2.lineColor = "#0000FF";
		graph2.lineThickness = 1;
		graph2.valueField = "k";
		graph2.connect = false;
		chart.addGraph(graph2);
		
		graph3 = new AmCharts.AmGraph();
		graph3.title = "D";
	//	graph3.lineColor = "#FF0000";
		graph3.lineThickness = 1;
		graph3.valueField = "d";
		graph3.connect = false;
		chart.addGraph(graph3);
		
		// GUIDES are vertical (can also be horizontal) lines (or areas) marking some event.
		var guide1 = new AmCharts.Guide();
		guide1.value = "20";
		guide1.lineColor = "#0000FF";
		guide1.lineAlpha = 1;
		guide1.dashLength = 2;
		guide1.lineThickness = 2;
		guide1.inside = true;
		guide1.labelRotation = 0;
	//	guide1.label = "20";
		valueAxis.addGuide(guide1);
		
		var guide2 = new AmCharts.Guide();
		guide2.value = "80";
		guide2.lineColor = "#0000FF";
		guide2.lineAlpha = 1;
		guide2.dashLength = 2;
		guide2.lineThickness = 2;
		guide2.inside = true;
		guide2.labelRotation = 0;
		guide2.label = "40";
		valueAxis.addGuide(guide2);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
		var legend = new AmCharts.AmLegend();
		chart.addLegend(legend);
	
		// WRITE
		chart.write(div);
	}
	
	function WMR7_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = false;
	//	valueAxis.minimum = 0;
	//	valueAxis.maximum = 100;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph1 = new AmCharts.AmGraph();
		graph1.title = "Williams";
	//	graph1.lineColor = "#FF0000";
		graph1.lineThickness = 1;
		graph1.valueField = "williams";
		graph1.connect = false;
		chart.addGraph(graph1);
		
		// GUIDES are vertical (can also be horizontal) lines (or areas) marking some event.
		var guide1 = new AmCharts.Guide();
		guide1.value = "-20";
		guide1.lineColor = "#0000FF";
		guide1.lineAlpha = 1;
		guide1.dashLength = 2;
		guide1.lineThickness = 2;
		guide1.inside = true;
		guide1.labelRotation = 0;
	//	guide1.label = "-20";
		valueAxis.addGuide(guide1);
		
		var guide2 = new AmCharts.Guide();
		guide2.value = "-80";
		guide2.lineColor = "#0000FF";
		guide2.lineAlpha = 1;
		guide2.dashLength = 2;
		guide2.lineThickness = 2;
		guide2.inside = true;
		guide2.labelRotation = 0;
	//	guide2.label = "-80";
		valueAxis.addGuide(guide2);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
	//	var legend = new AmCharts.AmLegend();
	//	chart.addLegend(legend);
	
		// WRITE
		chart.write(div);
	}
	
	function CCI_Graph(div,config) {                
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.colors = "#990000 #2A0CD0 #B0DE09 #0D8ECF #CD0D74 #CC0000 #00CC00 #999999 #333333 #000000".split(' ');
		chart.dataProvider = chartData[config];
		chart.marginLeft = 10;
		chart.categoryField = "date";
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisAlpha = 0;
		valueAxis.inside = false;
	//	valueAxis.minimum = 0;
	//	valueAxis.maximum = 100;
		chart.addValueAxis(valueAxis);
		
		// GRAPH                
		graph1 = new AmCharts.AmGraph();
		graph1.title = "CCI";
	//	graph1.lineColor = "#FF0000";
		graph1.lineThickness = 1;
		graph1.valueField = "cci";
		graph1.connect = false;
		chart.addGraph(graph1);
		
		// GUIDES are vertical (can also be horizontal) lines (or areas) marking some event.
		var guide1 = new AmCharts.Guide();
		guide1.value = "100";
		guide1.lineColor = "#0000FF";
		guide1.lineAlpha = 1;
		guide1.dashLength = 2;
		guide1.lineThickness = 2;
		guide1.inside = true;
		guide1.labelRotation = 0;
	//	guide1.label = "100";
		valueAxis.addGuide(guide1);
		
		var guide2 = new AmCharts.Guide();
		guide2.value = "-100";
		guide2.lineColor = "#0000FF";
		guide2.lineAlpha = 1;
		guide2.dashLength = 2;
		guide2.lineThickness = 2;
		guide2.inside = true;
		guide2.labelRotation = 0;
	//	guide2.label = "-100";
		valueAxis.addGuide(guide2);
		
		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
		// LEGEND                
	//	var legend = new AmCharts.AmLegend();
	//	chart.addLegend(legend);
	
		// WRITE
		chart.write(div);
	}
	
	function Volume_Graph(div,config) {
		// SERIAL CHART
	    chart = new AmCharts.AmSerialChart();
	    chart.dataProvider = chartData[config];
	    chart.categoryField = "date";
	    chart.startDuration = 1;
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.marginLeft = 10;
		chart.backgroundColor = "#0040FF";
		chart.backgroundAlpha = 0.05;
	//	chart.backgroundImage = "../../images/watermark_400.png";
		chart.fontSize = 8;
		
	    // AXES
	    // category
	    var categoryAxis = chart.categoryAxis;
	    categoryAxis.labelRotation = 90;
	    categoryAxis.gridPosition = "start";
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.gridAlpha = 0;
	
	    // value
	    // in case you don't want to change default settings of value axis,
	    // you don't need to create it, as one value axis is created automatically.
	    
	    // GRAPH
	    var graph = new AmCharts.AmGraph();
	    graph.valueField = "volume";
	    graph.connect = false;
	    graph.type = "column";
	    graph.lineAlpha = 0;
	    graph.fillAlphas = 0.8;
	    chart.addGraph(graph);

		// CURSOR
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorAlpha = 0;
		chartCursor.cursorPosition = "mouse";
		chartCursor.categoryBalloonDateFormat = "DD MMM";
		chart.addChartCursor(chartCursor);
		
    	chart.write(div);
	}
	
	function Candlestick_Graph(div,config) {
		// SERIAL CHART
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.dataProvider = chartData[config];
		chart.categoryField = "date";
		chart.marginTop = 0;
		chart.marginRight = 10;    
		chart.addListener('init', zoomCandlestick);
		chart.fontSize = 8;
		
		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set this to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to "DD"
		categoryAxis.dashLength = 1;
		categoryAxis.tickLenght = 0;
		categoryAxis.inside = false;
		
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.dashLength = 1;
		valueAxis.axisAlpha = 0;
		chart.addValueAxis(valueAxis);
		
		// GRAPH
		graph = new AmCharts.AmGraph();
		graph.title = "Candlestick:";
		graph.type = "candlestick";
		graph.lineColor = "#7f8da9";
		graph.fillColors = "#7f8da9";
		graph.negativeLineColor = "#db4c3c";
		graph.negativeFillColors = "#db4c3c";
		graph.fillAlphas = 1;
		graph.openField = "open";
		graph.highField = "high";
		graph.lowField = "low";
		graph.closeField = "close";
		// this will be used by scrollbar's graph, as we force it to "line" type
		graph.valueField = "close";
		chart.addGraph(graph);
		
		// CURSOR                
		var chartCursor = new AmCharts.ChartCursor();
		chart.addChartCursor(chartCursor);
		
		// SCROLLBAR
		var chartScrollbar = new AmCharts.ChartScrollbar();
		chartScrollbar.scrollbarHeight = 30;
		chartScrollbar.graph = graph; // as we want graph to be displayed in the scrollbar, we set graph here
		chartScrollbar.graphType = "line"; // we don't want candlesticks to be displayed in the scrollbar                
		chartScrollbar.gridCount = 4;
		chartScrollbar.color = "#FFFFFF";
		chartScrollbar.backgroundColor = "#D4D4D4";
		chartScrollbar.selectedBackgroundColor = "#EFEFEF";
		chart.addChartScrollbar(chartScrollbar);
		
		// WRITE
		chart.write(div);
	}
	
	function OHLC_Graph(div, config) {
		// SERIAL CHART
		chart = new AmCharts.AmSerialChart();
		chart.pathToImages = "amstockcharts/amcharts/images/";
		chart.marginRight = 15;
		chart.marginTop = 0;    
		chart.autoMarginOffset = 5;          
		chart.dataProvider = chartData[config];
		chart.plotAreaBorderColor = "#DADADA";
		chart.plotAreaBorderAlpha = 1;
		chart.categoryField = "date";
		chart.fontSize = 8;
		
		// AXES
		// Category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
		categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD                
		categoryAxis.tickLenght = 0;
		categoryAxis.axisColor = "#DADADA";
		
		// value            
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.axisColor = "#DADADA";
		chart.addValueAxis(valueAxis);    
		
		// GRAPH
		graph = new AmCharts.AmGraph();
		graph.title = "Price:";
		graph.type = "ohlc";
		graph.lineColor = "#7f8da9";
		graph.fillColors = "#7f8da9";
		graph.negativeLineColor = "#db4c3c";
		graph.negativeFillColors = "#db4c3c";
		// candlestick graph has 4 fields - open, low, high, close
		graph.openField = "open";
		graph.highField = "high";
		graph.lowField = "low";
		graph.closeField = "close";
		// this will be used by scrollbar's graph, as we force it to "line" type
		graph.valueField = "close";
		chart.addGraph(graph);
		
		// CURSOR                
		var chartCursor = new AmCharts.ChartCursor();
		chart.addChartCursor(chartCursor);
		
		// SCROLLBAR
		var chartScrollbar = new AmCharts.ChartScrollbar();
		chartScrollbar.scrollbarHeight = 30;
		chartScrollbar.graph = graph;
		chartScrollbar.graphType = "line";
		chartScrollbar.gridCount = 4;
		chartScrollbar.color = "#FFFFFF";
		chart.addChartScrollbar(chartScrollbar);
		
		// WRITE
		chart.write(div);
	}
	
	/**
	 * internal functions
	 * the following functions are interal functions to support data retrieval or graph building
	 */
	function zoomCandlestick() {
    	// different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues;
    	chart.zoomToIndexes(68, 134);
	}
	
	function reportError(div, xhr, ajaxOptions, thrownError) {
		alert(xhr.status);
		alert(thrownError);
	//	$(".contenttitle").after("<div id='reportError'>");
	//	$('#reportError').html("An error has occured while processing data\n"+xhr.status+"\n"+thrownError);
	//	$('#reportError').css({"color":"red","font-weight":"bold","height":"100px"});
	}

	function intradayDate(input) {
		var tijdblok = input.split(":");
		var d = new Date();
		d.setDate(d.getDate());
		d.setHours(tijdblok[0], tijdblok[1], 0, 0);
		return d;
	}
	
	function MultidayDate(input) {
		// input string -> 061320131336 [mmddyyyyhhmm]
		var month = Number( input.substring(0,2) );
		var day = Number( input.substring(2,4) );
		var year = Number( input.substring(4,8) );
		var hours = Number( input.substring(8,10) ); 
		var minutes = Number( input.substring(10,12) );
		
		var date = new Date(year, month-1, day, hours, minutes, 0, 0);
		return date;
	}
	
	function rsiDate(input) {
		var dateblok = input.split("/");
		var year = Number(dateblok[2]);
		var month = Number(dateblok[1]);
		var day = Number(dateblok[0]);
		var date = new Date(year, month - 1, day);
		return date;
	}
	
	function TAdate(input) {
		// input string -> 20130516 [yyyymmdd]
		var year = Number( input.substring(0,4) );
		var month = Number( input.substring(4,6) );
		var day = Number( input.substring(6,8) );
		var date = new Date(year, month-1, day);
		return date;
	}



