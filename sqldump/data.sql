-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Machine: localhost
-- Genereertijd: 27 nov 2014 om 12:07
-- Serverversie: 5.5.40-0ubuntu0.14.04.1
-- PHP-versie: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `stocks`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `data`
--

CREATE TABLE IF NOT EXISTS `data2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stockid` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `date` date NOT NULL,
  `value` decimal(8,6) NOT NULL COMMENT 'slotkoers',
  `sma5` decimal(8,6) NOT NULL,
  `ema5` decimal(8,6) NOT NULL,
  `sma10` decimal(8,6) NOT NULL,
  `ema10` decimal(8,6) NOT NULL,
  `sma15` decimal(8,6) NOT NULL,
  `ema15` decimal(8,6) NOT NULL,
  `sma20` decimal(8,6) NOT NULL,
  `ema20` decimal(8,6) NOT NULL,
  `sma25` decimal(8,6) NOT NULL,
  `ema25` decimal(8,6) NOT NULL,
  `sma30` decimal(8,6) NOT NULL,
  `ema30` decimal(8,6) NOT NULL,
  `sma35` decimal(8,6) NOT NULL,
  `ema35` decimal(8,6) NOT NULL,
  `sma40` decimal(8,6) NOT NULL,
  `ema40` decimal(8,6) NOT NULL,
  `sma45` decimal(8,6) NOT NULL,
  `ema45` decimal(8,6) NOT NULL,
  `sma50` decimal(8,6) NOT NULL,
  `ema50` decimal(8,6) NOT NULL,
  `sma60` decimal(8,6) NOT NULL,
  `ema60` decimal(8,6) NOT NULL,
  `sma70` decimal(8,6) NOT NULL,
  `ema70` decimal(8,6) NOT NULL,
  `sma80` decimal(8,6) NOT NULL,
  `ema80` decimal(8,6) NOT NULL,
  `sma90` decimal(8,6) NOT NULL,
  `ema90` decimal(8,6) NOT NULL,
  `sma100` decimal(8,6) NOT NULL,
  `ema100` decimal(8,6) NOT NULL,
  `sma120` decimal(8,6) NOT NULL,
  `ema120` decimal(8,6) NOT NULL,
  `sma140` decimal(8,6) NOT NULL,
  `ema140` decimal(8,6) NOT NULL,
  `sma160` decimal(8,6) NOT NULL,
  `ema160` decimal(8,6) NOT NULL,
  `sma180` decimal(8,6) NOT NULL,
  `ema180` decimal(8,6) NOT NULL,
  `sma200` decimal(8,6) NOT NULL,
  `ema200` decimal(8,6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stockid` (`stockid`),
  KEY `day` (`date`),
  KEY `time` (`time`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
