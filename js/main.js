$(function() {
      loading=function(show){
          if($('#loading').length){
               $('#loading').css('display',(show?'inline':'none'));
          }
      }
      $('.sh').on('click',function(){
          var id = $(this).attr('id');
          if($(this).hasClass('btn-primary')){
              $('.'+id).addClass('hidden');
              $(this).removeClass('btn-primary')
          }
          else{
               $('.'+id).removeClass('hidden');
               $(this).addClass('btn-primary')
          }
          var state = [];
          $('.btns .btn').each(function(){
              if($(this).hasClass('btn-primary')){
                  state.push($(this).attr('id'))
              }
          })
          state=state.join(',');
          $.cookie("state", state, { expires: 14 });
      })
      
     var state = $.cookie("state");
     if(state){
         state = state.split(',');
         for(var i in state){
             $('#'+state[i]).click();
         }
     }
     
     
     $('input.alert').on('blur',function(){
       var val =  $(this).val();
       if(!val.match(/^\d+,\d+$/)){ $(this).val('');};
       
     })
     $('#update-alerts').on('click',function(){
        $(this).addClass('disabled');
        var alerts = {};
        $('input.alert').each(function(){
            var val = $(this).val();
            var id = $(this).attr('id');
            alerts[id]=val;
        })
        var alerts = $.toJSON(alerts);
        var base = $('#base').val();
        $.post(base+'admin/update_alerts',{
              alerts:alerts
        },
        function(data,status){
            $('#update-alerts').removeClass('disabled');
        });
         
     })
     $('#clear-alerts').on('click',function(){
         $('input.alert').each(function(){
             $(this).val('');
         })
      
     })
     
     $('#set-alerts-m1').on('click',function(){
         $('input.prijs').each(function(){
            var prijs =  $(this).val();
            prijs = prijs.replace(/,/,'.')
            prijs = prijs-0;
            prijs = prijs*0.99;
            prijs = prijs*1000;
            prijs = Math.round(prijs)
            prijs = prijs/1000;
            prijs = String(prijs);
            prijs = prijs.replace(/\./,',');
            var id= $(this).attr('id');
            id = id.replace(/^p/,'l');
            $('#'+id).val(prijs)
         })
     })
     
     $('#set-alerts-m2').on('click',function(){
         $('input.prijs').each(function(){
            var prijs =  $(this).val();
            prijs = prijs.replace(/,/,'.')
            prijs = prijs-0;
            prijs = prijs*0.98;
            prijs = prijs*1000;
            prijs = Math.round(prijs)
            prijs = prijs/1000;
            prijs = String(prijs);
            prijs = prijs.replace(/\./,',');
            var id= $(this).attr('id');
            id = id.replace(/^p/,'l');
            $('#'+id).val(prijs)
         })
     })
     $('#set-alerts-h1').on('click',function(){
         $('input.prijs').each(function(){
            var prijs =  $(this).val();
            prijs = prijs.replace(/,/,'.')
            prijs = prijs-0;
            prijs = prijs*1.01;
            prijs = prijs*1000;
            prijs = Math.round(prijs)
            prijs = prijs/1000;
            prijs = String(prijs);
            prijs = prijs.replace(/\./,',');
            var id= $(this).attr('id');
            id = id.replace(/^p/,'h');
            $('#'+id).val(prijs)
         })
     })
     
     $('#set-alerts-h2').on('click',function(){
         $('input.prijs').each(function(){
            var prijs =  $(this).val();
            prijs = prijs.replace(/,/,'.')
            prijs = prijs-0;
            prijs = prijs*1.02;
            prijs = prijs*1000;
            prijs = Math.round(prijs)
            prijs = prijs/1000;
            prijs = String(prijs);
            prijs = prijs.replace(/\./,',');
            var id= $(this).attr('id');
            id = id.replace(/^p/,'h');
            $('#'+id).val(prijs)
         })
     })
     
    $('.btn-nav').on('click',function(){
           var base = $('#base').val();
           var url  = $(this).val();
           document.location = base+$(this).val();
    })
    
    
    // EDIT YAHOO TICKERS
    $('.input-small.sy').on('change',function(){
           var base = $('#base').val();
           var id  = $(this).attr('id');
           id = id.replace(/sy_/,'');
           var value  = $(this).val();
           $.post(base+'admin/update_stocks',{
              function:'yahooticker',
              id:id,
              value:value
           },
           function(data,status){
               
           });
    })
    // EDIT NAME
    $('.input-medium.sn').on('change',function(){
           var base = $('#base').val();
           var id  = $(this).attr('id');
           id = id.replace(/sn_/,'');
           var value  = $(this).val();
           $.post(base+'admin/update_stocks',{
              function:'name',
              id:id,
              value:value
           },
           function(data,status){
               
           });
    })
    // EDIT BEURS.NL NAME
    $('.input-medium.sb').on('change',function(){
           var base = $('#base').val();
           var id  = $(this).attr('id');
           id = id.replace(/sb_/,'');
           var value  = $(this).val();
           $.post(base+'admin/update_stocks',{
              function:'beursnl',
              id:id,
              value:value
           },
           function(data,status){
               
           });
    })
    // DELETE STOCK
    $('.delstock').on('click',function(){
        if(!confirm('Delete?')){return;}
           loading(1);
           var elm  = $(this).parents('tr');
           var base = $('#base').val();
           var id  = $(this).attr('id');
           id = id.replace(/delstock_/,'');
           $.post(base+'admin/update_stocks',{
              function:'deletestock',
              id:id
           },
           function(data,status){  
              loading(0);
              elm.html('')  
           });
    })
    
    // STATUS STOCK
    $('.ba').on('click',function(){
           var elm  = $(this);
           var newstatus = $(this).hasClass('btn-success')?'':'Actief';
           var base = $('#base').val();
           var id  = $(this).attr('id');
           id = id.replace(/ba_/,'');
           $.post(base+'admin/update_stocks',{
              function:'setactive',
              id:id,
              status:newstatus
           },
           function(data,status){  
              elm.toggleClass('btn-success');
              elm.blur();
           });
    })
    
    
    
    // ADD STOCK
    $('#addstock').on('click',function(){
          // loading(1);
           var elm  = $(this);
           var ticker = $('#new-stock-ticker').val();
           if(!ticker){return;}
           var base = $('#base').val();
          
           $.post(base+'admin/update_stocks',{
              function:'newticker',
              ticker:ticker,
           },
           function(data,status){  
              //loading(0);
              alert(data)
              location.reload();
           });
           //loading(0);
    })
    
    $('.yho-out').on('click',function(){
        var elm  = $(this);
        var base = $('#base').val();
        var val = elm.val();
        window.open('https://uk.finance.yahoo.com/q?s='+val);
    })
    
    if($('#select-stock').length){
        $(document).on('keyup',function(event) {
           var key = event.keyCode;
           if(key==100||key==102){
                var elm = $('.group-timespan.btn-primary');
                if(key==100&&elm.prev()){
                    elm.prev().click();
                }
                if(key==102&&elm.next()){
                    elm.next().click(); 
                }        
           }
           if(key==98||key==104){
                var elm = $('#select-stock a.selected');
                if(key==104&&elm.parent().prevAll('li.on:first').find('a')){
                    elm.parent().prevAll('li.on:first').find('a').click();
                    elm.parent().prevAll('li.on:first').find('a')[0].scrollIntoView();
                }
                if(key==98&&elm.parent().nextAll('li.on:first').find('a')){
                   elm.parent().nextAll('li.on:first').find('a').click(); 
                   elm.parent().nextAll('li.on:first').find('a')[0].scrollIntoView();
                }
           }
           event.preventDefault();
        });
    }
    

    // selecteer de average range
    $('.group-range').on('click',function(){
        if($(this).hasClass('btn-primary')){
              $(this).removeClass('btn-primary');
        }
        else{
            if($('.group-range.btn-primary').length>=8){
                $('.group-range.btn-primary').removeClass('btn-primary');
            }
            $(this).addClass('btn-primary');
        }
        
        var ar  = [];
        $('.group-range.btn-primary').each(function(){
            ar.push($(this).index())
        })
        var sma = ar.join('.');
        $.cookie('graph_range', sma, {expires:365});
        
        
        get_graph();
    })
    
    // selecteer de timespan
    $('.group-timespan').on('click',function(){
        $('.group-timespan').removeClass('btn-primary'); 
        $(this).addClass('btn-primary');
        $.cookie('graph_timespan',$(this).index(), {expires:365});
        get_graph();
    })
    

    
    // selecteer het stock mvb listitem
    
    $('#select-stock a').on('click',function(event){
        event.preventDefault();
        var elm = $(this);    
        if(!elm.hasClass('off')){
            $('#select-stock a').removeClass('selected');
            var index = elm.index('#select-stock a')
            $.cookie('graph_stock',index,{expires:365});
            elm.addClass('selected');
            get_graph();
        }
    })

    // in/deactiveer stock mvb listitem
    
    $('#select-stock i').on('click',function(event){
        var elm = $(this);    
        var newstatus = 
        (elm.hasClass('icon-eye-open'))?'':'Actief';
        var base = $('#base').val();
        var id   = elm.attr('id');
        id = id.replace(/id1_/,'');
        $.post(base+'admin/update_stocks',{
              function:'setactive',
              id:id,
              status:newstatus
           },
           function(data,status){
              elm.toggleClass('icon-eye-open');
              elm.toggleClass('icon-eye-close');
              elm.next().toggleClass('off');
              elm.next().toggleClass('on');
              elm.next().parent().toggleClass('off');
              elm.next().parent().toggleClass('on');
           });
    })
    
    graph_init = function(){
        var index;
        if($.cookie('graph_range')){
            var tmp = $.cookie('graph_range');
            var ids = tmp.split('.');
            for(var i in ids){
                $('.group-range:eq('+ids[i]+')').addClass('btn-primary');
            }
        }
        if(!$.cookie('graph_timespan')){
           $.cookie('graph_timespan', '1', {expires:365});
        }
        index = $.cookie('graph_timespan');
        $('.group-timespan:eq('+index+')').addClass('btn-primary')
        
        if(!$.cookie('graph_stock')){
           $.cookie('graph_stock', '0', {expires:365});
        }
        index = $.cookie('graph_stock');
        $('#select-stock a:eq('+index+')').addClass('selected');  
        $('#select-stock a:eq('+index+')')[0].scrollIntoView();
        get_graph(); 
    }

  
    get_graph = function(){
        var base      = $('#base').val();
        // stock
        var elm = $('#select-stock a.selected');
        var stockid = elm.attr('id');
        stockid = stockid.replace(/id2_/,'');
        var stockname = elm.text();
     
        var timespan  = $('.group-timespan.btn-primary').val();
        stockname     = stockname.toLowerCase()

        var ar  = [];
        $('.group-range.btn-primary').each(function(){
            ar.push($(this).val())
        })
        var sma = ar.join('.');
        $.post(base + 'admin/get_data', {
            function:'get_data',
            stockid:stockid,
            stockname:stockname,
            timespan:timespan,
            sma:sma
        },
        function (data, status) {
           alert(data)
            var tmp = $.parseJSON(data)
            var graph = tmp['graph'];
            var count = tmp['count'];
            var min = tmp['min'];
            var max = tmp['max'];
            var xaxis = tmp['xaxis'];
            
            var settings = {
                series: {
                    lines: { 
                        show: true ,
                        lineWidth:1.5
                    },
                    points: { show: false }
                    
                },
                xaxis: {
                    ticks: xaxis,
                },
                yaxis: {
                    ticks: 10,
                    min: min,
                    max: max,
                    tickDecimals: 2
                },
                grid: {
                    backgroundColor: { colors: [ "#fff", "#eee" ] },
                    borderWidth: {
                        top: 1,
                        right: 1,
                        bottom: 2,
                        left: 2
                    }
                },legend: {
                    show: 1,
//                    backgroundColor: '#cfcfcf',
//                    backgroundOpacity: 1
                    
                }
            }
            
            
            $.plot("#graph",graph,settings);


        });
    }
    graph_init();
});





	

	